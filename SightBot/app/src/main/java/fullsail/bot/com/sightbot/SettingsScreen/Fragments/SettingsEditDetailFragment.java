package fullsail.bot.com.sightbot.SettingsScreen.Fragments;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;
import fullsail.bot.com.sightbot.CustomClasses.ImageCustomClass;
import fullsail.bot.com.sightbot.CustomClasses.KidCustomClass;
import fullsail.bot.com.sightbot.DataHelper;
import fullsail.bot.com.sightbot.GlobalVariables;
import fullsail.bot.com.sightbot.R;
import fullsail.bot.com.sightbot.RegisterBoolean;
import fullsail.bot.com.sightbot.SettingsScreen.SettingsActivity;

/**
 * Created by snipetysnipes on 3/7/16.
 */
public class SettingsEditDetailFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = "LoginFragment.TAG";
    private static final int TAKE_PICTURE_REQUEST = 100;
    private static final int GET_PICTURE_REQUEST = 50;
    private KidCustomClass kid;
    String childName;
    ImageReceiver receiver;
    Bitmap imageOfChild;
    boolean imageIsFromCamera;

    public static SettingsEditDetailFragment newInstance(KidCustomClass childBeingEdited){
        SettingsEditDetailFragment frag = new SettingsEditDetailFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("childBeingEdited", childBeingEdited);
            frag.setArguments(bundle);

        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_settings_edit_detail, container, false);

        Spinner genderSpinner = (Spinner) view.findViewById(R.id.genderSpinner);
        Spinner weekSpinner = (Spinner) view.findViewById(R.id.weekSpinner);
        Spinner quarterSpinner = (Spinner) view.findViewById(R.id.quarterSpinner);
        Spinner gradeSpinner = (Spinner) view.findViewById(R.id.gradeSpinner);
        Spinner titleSpinner = (Spinner) view.findViewById(R.id.titleSpinner);
        Switch parentSwitch = (Switch) view.findViewById(R.id.parentEmail);

        view.findViewById(R.id.picture_fromCamera).setOnClickListener(this);
        view.findViewById(R.id.picture_fromGallary).setOnClickListener(this);
        view.findViewById(R.id.picture_fromDefault).setOnClickListener(this);

        parentSwitch.setChecked(true);

        ArrayAdapter<CharSequence> genderSpinnerAdapter= ArrayAdapter.createFromResource(
                getActivity(),
                R.array.genderArray,
                android.R.layout.simple_dropdown_item_1line
        );

        ArrayAdapter<CharSequence> weekSpinnerAdapter = ArrayAdapter.createFromResource(
                getActivity(),
                R.array.weekArray,
                android.R.layout.simple_dropdown_item_1line
        );

        ArrayAdapter<CharSequence> gradeSpinnerAdapter = ArrayAdapter.createFromResource(
                getActivity(),
                R.array.gradeArray,
                android.R.layout.simple_dropdown_item_1line
        );

        ArrayAdapter<CharSequence> quarterSpinnerAdapter = ArrayAdapter.createFromResource(
                getActivity(),
                R.array.quarterArray,
                android.R.layout.simple_dropdown_item_1line
        );

        ArrayAdapter<CharSequence> titleSpinnerAdapter = ArrayAdapter.createFromResource(
                getActivity(),
                R.array.titleArray,
                android.R.layout.simple_dropdown_item_1line
        );

        genderSpinner.setAdapter(genderSpinnerAdapter);
        weekSpinner.setAdapter(weekSpinnerAdapter);
        gradeSpinner.setAdapter(gradeSpinnerAdapter);
        quarterSpinner.setAdapter(quarterSpinnerAdapter);
        titleSpinner.setAdapter(titleSpinnerAdapter);

        if(getArguments().getSerializable("childBeingEdited") != null){
            KidCustomClass kidDetails = (KidCustomClass)getArguments().getSerializable("childBeingEdited");
            if(kidDetails.getGender().equals("Male")) {
                genderSpinner.setSelection(0);
            }else {
                genderSpinner.setSelection(1);
            }
            weekSpinner.setSelection(kidDetails.getCurrentWeek()-1);
            quarterSpinner.setSelection(kidDetails.getCurrentQuarter()-1);
            String[] grades = getResources().getStringArray(R.array.gradeArray).clone();
            for(int i = 0; i < grades.length; i++){
                if(kidDetails.getGrade().equals(grades[i])){
                    gradeSpinner.setSelection(i);
                }
            }
            String[] titles = getResources().getStringArray(R.array.titleArray).clone();
            for(int i = 0; i < titles.length; i++){
                if(kidDetails.getTeacherTitle().equals(titles[i])){
                    titleSpinner.setSelection(i);
                }
            }
            parentSwitch.setChecked(kidDetails.isParentEmailUtilized());
            ((EditText)view.findViewById(R.id.name)).setText(kidDetails.getChildName());
            ((EditText)view.findViewById(R.id.teacherEmail)).setText(kidDetails.getTeacherEmail());
            ((EditText)view.findViewById(R.id.teacher)).setText(kidDetails.getTeacherName());
        }

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.settings_detail_child_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.do_not_save_child){
            if(DataHelper.getAllKids(getActivity()).size() != 0) {

                //The changes where canceled and child accounts already exist
                //TODO - Load the current child's detail fragment not editable
                SettingsDetailFragment fragment;
                if(getArguments().getSerializable("childBeingEdited") != null){

                    //The user was editing anc cancelled load that child's account details
                    fragment = SettingsDetailFragment.newInstance((KidCustomClass)getArguments().getSerializable("childBeingEdited"));
                } else {

                    //The user was creating a new Account and cancelled load the first child's details
                    fragment = SettingsDetailFragment.newInstance(DataHelper.getAllKids(getActivity()).get(0));
                }

                getActivity().unregisterReceiver(receiver);
                getFragmentManager().beginTransaction().replace(R.id.details,fragment).commit();

            } else {

                getActivity().unregisterReceiver(receiver);
                SettingsEditDetailFragment fragment = newInstance(null);
                getFragmentManager().beginTransaction().replace(R.id.details,fragment).commit();
            }

            return true;

        } else if (item.getItemId() == R.id.save_child){
//            getActivity().unregisterReceiver(receiver);
             if(saveOrUpdateChildDetails()){
                 ((SettingsActivity)getActivity()).childDetailsSaved(kid);
             }

            return true;
        }

        RegisterBoolean.booleanCheck(true);

        return false;
    }

    private boolean saveOrUpdateChildDetails(){
        EditText childNameEditTextField = (EditText)getActivity().findViewById(R.id.name);
        String childName = childNameEditTextField.getText().toString();
        String teacherTitle = ((Spinner) getActivity().findViewById(R.id.titleSpinner)).getSelectedItem().toString();
        String teacherName = ((EditText) getActivity().findViewById(R.id.teacher)).getText().toString();
        String teacherEmail = ((EditText) getActivity().findViewById(R.id.teacherEmail)).getText().toString();
        String gender = ((Spinner) getActivity().findViewById(R.id.genderSpinner)).getSelectedItem().toString();
        String grade = ((Spinner) getActivity().findViewById(R.id.gradeSpinner)).getSelectedItem().toString();
        boolean parentWantsEmails = ((Switch) getActivity().findViewById(R.id.parentEmail)).isChecked();
        int currentWeek = Integer.parseInt(((Spinner) getActivity().findViewById(R.id.weekSpinner)).getSelectedItem().toString());
        int currentQuarter = Integer.parseInt(((Spinner) getActivity().findViewById(R.id.quarterSpinner)).getSelectedItem().toString());

        Vibrator vibrator = (Vibrator)getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        if(childName.equals("")){
            Toast.makeText(getActivity(), "Please do not leave child name blank", Toast.LENGTH_SHORT).show();
            vibrator.vibrate(500);
            childNameEditTextField.setBackgroundColor(getResources().getColor(android.R.color.holo_red_light));
            return false;
        }

        if(imageOfChild == null){
            Toast.makeText(getActivity(), "Please select a photo for your child", Toast.LENGTH_SHORT).show();
            return false;
        }


        if (getArguments().getSerializable("childBeingEdited") != null) {

            //Update a child's account data
            kid = new KidCustomClass(childName, gender, grade, teacherName, teacherEmail, currentWeek, currentQuarter, parentWantsEmails, teacherTitle, teacherName/*, imageOfChild, imageIsFromCamera*/);
            ImageCustomClass image = new ImageCustomClass(imageOfChild, imageIsFromCamera, childName);
            DataHelper.saveNewKid(getActivity(), kid);
            //DataHelper.saveNewImage(getActivity(), image);
            DataHelper.updateImage(getActivity(), image);

        } else {
            if (DataHelper.getSpecificKid(getActivity(), childName) != null) {

                //The user is attempting to save a new child with the same name
                vibrator.vibrate(500);
                childNameEditTextField.setBackgroundColor(getResources().getColor(android.R.color.holo_red_light));
                Toast.makeText(getActivity(), "A child with the name " + childName + " already exists.", Toast.LENGTH_LONG).show();
                return false;

            } else {

                //This saves the new child account
                kid = new KidCustomClass(childName, gender, grade, teacherName, teacherEmail, currentWeek, currentQuarter, parentWantsEmails, teacherTitle, teacherName/*, imageOfChild, imageIsFromCamera*/);
                ImageCustomClass image = new ImageCustomClass(imageOfChild, imageIsFromCamera, childName);
                DataHelper.saveNewKid(getActivity(), kid);
                DataHelper.saveNewImage(getActivity(), image);
            }
        }
        return true;
    }

    private boolean checkChildNameField(){

        String childName = ((EditText)getActivity().findViewById(R.id.name)).getText().toString();

        if(childName.equals("")){
            Toast.makeText(getActivity(), "Please do not leave child name blank", Toast.LENGTH_SHORT).show();
            Vibrator vibrator = (Vibrator)getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(500);
            getActivity().findViewById(R.id.name).setBackgroundColor(getResources().getColor(android.R.color.holo_red_light));
            return false;

        } else {
            return true;
        }
    }

    @Override
    public void onClick(View v) {

        if(checkChildNameField()){
            childName = ((EditText)getActivity().findViewById(R.id.name)).getText().toString();
        } else {
            return;
        }

        switch (v.getId()){
            case R.id.picture_fromCamera:
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                getActivity().startActivityForResult(intent, TAKE_PICTURE_REQUEST);
                imageIsFromCamera = true;
                break;
            case R.id.picture_fromGallary:
                intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                getActivity().startActivityForResult(intent,GET_PICTURE_REQUEST);
                imageIsFromCamera = false;
                break;
            case R.id.picture_fromDefault:

                if (((Spinner) getActivity().findViewById(R.id.genderSpinner)).getSelectedItem().toString().equals("Male")) {
                    imageOfChild = BitmapFactory.decodeResource(getResources(), R.drawable.little_boy);
                    ((ImageView) getActivity().findViewById(R.id.detailImage)).setImageBitmap(imageOfChild);
                    imageIsFromCamera = true;
                } else {
                    imageOfChild = BitmapFactory.decodeResource(getResources(), R.drawable.little_girl);
                    ((ImageView) getActivity().findViewById(R.id.detailImage)).setImageBitmap(imageOfChild);
                    imageIsFromCamera = true;

                }

                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

            IntentFilter filter = new IntentFilter(GlobalVariables.BROADCAST_IMAGE_TAKEN);
            receiver = new ImageReceiver();
            getActivity().registerReceiver(receiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();

        if (!RegisterBoolean.registerBool) {
           // getActivity().unregisterReceiver(receiver);
        }
    }

    private class ImageReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent != null){
                if(intent.hasExtra(GlobalVariables.EXTRA_IMAGE_LOCATION_AS_STRING)){
                    imageOfChild = BitmapFactory.decodeFile(intent.getStringExtra(GlobalVariables.EXTRA_IMAGE_LOCATION_AS_STRING));
                } else {
                    Bundle extras = intent.getExtras();
                    imageOfChild = (Bitmap) extras.get("data");
                }
            }
        }

    }
}