package fullsail.bot.com.sightbot.LessonScreen.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import fullsail.bot.com.sightbot.CustomClasses.LessonProgressCustomClass;
import fullsail.bot.com.sightbot.R;

/**
 * Created by davidfermer on 3/13/16.
 */
public class LessonProgressFragment extends Fragment {

    private static final String lessonProgress = "LessonProgress";
    ProgressBar progressBar;
    int color;
    LessonProgressInterface lessonProgressInterface;
    LessonProgressCustomClass progress;

    public LessonProgressFragment() {
    }

    public static LessonProgressFragment newInstance(LessonProgressCustomClass progress) {
        Bundle bundle = new Bundle();

        bundle.putSerializable(lessonProgress, progress);
        LessonProgressFragment frag = new LessonProgressFragment();
        frag.setArguments(bundle);

        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        progress = (LessonProgressCustomClass)getArguments().getSerializable(lessonProgress);
        View v = inflater.inflate(R.layout.progress_indicator_fragment, container, false);
        color = getResources().getColor(R.color.darkBlue);
        progressBar = (ProgressBar) v.findViewById(R.id.lesson_progressBar);
        progressBar.setProgress(progress.getNumberOfWordsRemaining());
        progressBar.setMax(progress.getOriginalNumberOfWords());
        progressBar.setProgressTintList(ColorStateList.valueOf(color));

        if(progress.getWordsRemaining() == 0){
            lessonProgressInterface.lessonComplete();
        }

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        lessonProgressInterface = (LessonProgressInterface) getActivity();

    }

    @Override
    public void onDetach() {
        super.onDetach();
        lessonProgressInterface = null;
    }

    public interface LessonProgressInterface{
        void lessonComplete();
    }
}
