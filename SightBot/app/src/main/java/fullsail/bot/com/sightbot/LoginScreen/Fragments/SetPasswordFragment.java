package fullsail.bot.com.sightbot.LoginScreen.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Vibrator;
import android.text.InputType;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import fullsail.bot.com.sightbot.SettingsScreen.SettingsActivity;
import fullsail.bot.com.sightbot.GlobalVariables;
import fullsail.bot.com.sightbot.R;

public class SetPasswordFragment extends Fragment implements View.OnClickListener,
        CompoundButton.OnCheckedChangeListener{

    public static final String TAG = "SetPassFrag.TAG";

    FrameLayout container;

    public SetPasswordFragment() {
    }

    public static SetPasswordFragment newInstance(){
        return new SetPasswordFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_password_set_password, container, false);
        view.findViewById(R.id.cancel_parentLogin).setOnClickListener(this);
        view.findViewById(R.id.login_Button).setOnClickListener(this);
        ((CheckBox)view.findViewById(R.id.checkboxShowPassword)).setOnCheckedChangeListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        container = (FrameLayout) getActivity().findViewById(R.id.passwordFragmentContainer);

    }

    @Override
    public void onClick(View v) {

        ImageView arrow = (ImageView) getActivity().findViewById(R.id.imageView2);

        if(v.getId() == R.id.login_Button){
            EditText emailAddressTextField = (EditText)getActivity().findViewById(R.id.parentEmailAddress);
            EditText firstPasswordField = (EditText)getActivity().findViewById(R.id.dialog_view_password_field);
            EditText secondPasswordField = (EditText)getActivity().findViewById(R.id.reenterPasswordField);
            EditText securityQuestionField = (EditText)getActivity().findViewById(R.id.securityQuestionField);
            EditText securityAnswerField = (EditText)getActivity().findViewById(R.id.securityAnswerField);


            //check email address validity
            boolean emailIsValid = Patterns.EMAIL_ADDRESS.matcher(emailAddressTextField.getText()).matches();
            if(!emailIsValid){
                Toast.makeText(getActivity(),"Please enter a valid Email Address", Toast.LENGTH_SHORT).show();
            }
            //check passwords match
            if(!firstPasswordField.getText().toString().equals(secondPasswordField.getText().toString())){
                Toast.makeText(getActivity(),"Your passwords do not match", Toast.LENGTH_SHORT).show();
            }

            if(securityQuestionField.getText().toString().equals("") || securityAnswerField.getText().toString().equals("")) {
                Toast.makeText(getActivity(), "You must fill out security question info", Toast.LENGTH_SHORT).show();
            }
            Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            //check passwords match and email is valid
            if(firstPasswordField.getText().toString().equals(secondPasswordField.getText().toString()) && emailIsValid &&
                    !securityQuestionField.getText().toString().equals("") && !securityAnswerField.getText().toString().equals("")){
                SharedPreferences sharedPreferences = getActivity()
                        .getSharedPreferences(GlobalVariables.Email, Context.MODE_PRIVATE);
                sharedPreferences.edit().putString(GlobalVariables.Email, emailAddressTextField.getText().toString()).apply();
                sharedPreferences = getActivity().getSharedPreferences(GlobalVariables.Password, Context.MODE_PRIVATE);
                sharedPreferences.edit().putString(GlobalVariables.Password, firstPasswordField.getText().toString()).apply();
                sharedPreferences.edit().putString(GlobalVariables.SECURITY_QUESTION_PREF, securityQuestionField.getText().toString()).apply();
                sharedPreferences.edit().putString(GlobalVariables.SECURITY_ANSWER_PREF, securityAnswerField.getText().toString()).apply();

                //Start Settings Activity

                getActivity().getFragmentManager().beginTransaction().remove(this).commit();
                container.setVisibility(View.GONE);
                Intent intent = new Intent(getActivity(), SettingsActivity.class);
                getActivity().overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
                startActivity(intent);

            } else {
                vibrator.vibrate(500);
            }
        } else if (v.getId() == R.id.cancel_parentLogin) {
//            arrow.setVisibility(View.VISIBLE);
//            getActivity().findViewById(R.id.hint).setVisibility(View.VISIBLE);
//            Animation animSlide = AnimationUtils.loadAnimation(getActivity(),
//                    R.anim.loop);
//            arrow.startAnimation(animSlide);
            getActivity().findViewById(R.id.passwordFragmentContainer).setVisibility(View.GONE);
            getFragmentManager().beginTransaction().remove(this).commit();
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked){
            ((TextView)getActivity().findViewById(R.id.dialog_view_password_field))
                    .setInputType(InputType.TYPE_CLASS_TEXT);
            ((TextView)getActivity().findViewById(R.id.reenterPasswordField))
                    .setInputType(InputType.TYPE_CLASS_TEXT);
        } else {
            ((TextView)getActivity().findViewById(R.id.dialog_view_password_field))
                    .setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);
            ((TextView)getActivity().findViewById(R.id.reenterPasswordField))
                    .setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
    }
}
