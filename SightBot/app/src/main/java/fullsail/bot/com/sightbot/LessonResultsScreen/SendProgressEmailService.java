package fullsail.bot.com.sightbot.LessonResultsScreen;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import fullsail.bot.com.sightbot.CustomClasses.GMailSender;
import fullsail.bot.com.sightbot.CustomClasses.KidCustomClass;
import fullsail.bot.com.sightbot.CustomClasses.LessonProgressCustomClass;
import fullsail.bot.com.sightbot.GlobalVariables;

/**
 * Created by davidfermer on 3/22/16.
 */
public class SendProgressEmailService extends IntentService {
    public SendProgressEmailService() {
        super("SendProgressEmailService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        LessonProgressCustomClass kidsProgress = (LessonProgressCustomClass)intent
                .getSerializableExtra(GlobalVariables.EXTRA_LESSON_PROGRESS);
        KidCustomClass kid = kidsProgress.getKid();
        GMailSender sender = new GMailSender();

        String messageSubject = kid.getChildName() +" completed a lesson in Sight Bot";
        String pronounForKid;

        if (kid.getGender().equals("Male")){
            pronounForKid = "he";
        } else {
            pronounForKid = "she";
        }

        String messageText = "Hello, " +
                System.getProperty("line.separator") +
                System.getProperty("line.separator") +
                "     " + kid.getChildName() + " completed the week " + kid.getCurrentWeek() +
                " lesson in quarter  " + kid.getCurrentQuarter() + "." +
                System.getProperty("line.separator") +
                System.getProperty("line.separator") +
                "     " + kid.getChildName() + " completed " +
                String.valueOf(kidsProgress.getOriginalNumberOfWords()) + " sight words.  " +
                kid.getChildName() + " had an overall success rate of " +
                String.valueOf(kidsProgress.getPercentage()) +" percent."
                + System.getProperty("line.separator") + System.getProperty("line.separator") + "     The following is a list of the words " +
                kid.getChildName() + " saw, how many times "+ pronounForKid +" saw them and how many times " + pronounForKid
                + " said them correctly." + System.getProperty("line.separator") + System.getProperty("line.separator");

        for (int i = 0; i < kidsProgress.getCompletedWordProgresses().size(); i++){
            LessonProgressCustomClass.LessonWordProgress singleWordProgress = kidsProgress.getCompletedWordProgresses().get(i);
            messageText = messageText + singleWordProgress.word + "  Seen " +
            String.valueOf(singleWordProgress.numberOfTimesSeenThisSession) + " times  Correct " +
            String.valueOf(singleWordProgress.numberOfTimesCorrectThisSession) + " time(s)." +
            System.getProperty("line.separator");
        }

        String emailAddressToSendTo = "";
        try{
            SharedPreferences sharedPreferences = this.getSharedPreferences(GlobalVariables.Email, Context.MODE_PRIVATE);
            String parentEmail = sharedPreferences.getString(GlobalVariables.Email, "");
            if(kid.isParentEmailUtilized() && !kid.getTeacherEmail().equals("")){
                emailAddressToSendTo = kid.getTeacherEmail() +", " + parentEmail;
            } else if(kid.isParentEmailUtilized()){
                emailAddressToSendTo = parentEmail;
            } else if(!kid.getTeacherEmail().equals("")){
                emailAddressToSendTo = kid.getTeacherEmail();
            }
            sender.sendMail(messageSubject,messageText,emailAddressToSendTo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
