package fullsail.bot.com.sightbot.SettingsScreen.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import fullsail.bot.com.sightbot.CustomClasses.ImageCustomClass;
import fullsail.bot.com.sightbot.CustomClasses.KidCustomClass;
import fullsail.bot.com.sightbot.DataHelper;
import fullsail.bot.com.sightbot.R;

/**
 * Created by snipetysnipes on 3/8/16.
 */
public class ChildListAdapter extends BaseAdapter {

    public static final String TAG = "Articles.TAG";
    public static final int ID_CONSTANT = 0x0100000;

    Context mContext;
    ArrayList<KidCustomClass> kArray;
    ArrayList<Bitmap> bitmaps;

    public ChildListAdapter(Context mContext, ArrayList<KidCustomClass> kArray) {
        this.mContext = mContext;
        this.kArray = kArray;
        //this.bitmaps = bitmaps;
    }

    @Override
    public int getCount() {
        if (kArray != null) {
            return kArray.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int i) {
        if (kArray != null && i < kArray.size() && i >= 0) {
            return kArray.get(i);
        } else {
            Log.i(TAG, "Error handling!");
            return null;
        }

    }

    @Override
    public long getItemId(int i) {
        if (kArray != null) {
            return ID_CONSTANT + i;
        } else {
            Log.i(TAG, "Returned 0");
            return 0;
        }
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.child_item_layout, parent, false);

        }

        KidCustomClass k = (KidCustomClass) getItem(i);

        TextView titleText = (TextView) convertView.findViewById(R.id.childName);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.childImage);


        titleText.setText(k.getChildName());
        ImageCustomClass imageCustomClass = DataHelper.getSpecificImage(mContext, k.getChildName());
        imageView.setImageBitmap(imageCustomClass.getChildImage());
//        if (k.getGender().equals("Male")) {
//            imageView.setImageResource(R.drawable.little_boy);
//        } else {
//            imageView.setImageResource(R.drawable.little_girl);
//        }

        return convertView;
    }
}
