package fullsail.bot.com.sightbot.LoginScreen.Fragments;


import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import fullsail.bot.com.sightbot.GlobalVariables;
import fullsail.bot.com.sightbot.R;

public class PasswordSecurityQuestionFragment extends Fragment {

    public static final String TAG = "PasswordSecurityQuestionFragment.TAG";

    SharedPreferences preferences;
    EditText answerText;

    public PasswordSecurityQuestionFragment() {
    }

    public static PasswordSecurityQuestionFragment newInstance() {
        return new PasswordSecurityQuestionFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_password_security_question, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        preferences = getActivity().getSharedPreferences(GlobalVariables.Password, Context.MODE_PRIVATE);

        Button doneButton = (Button) getView().findViewById(R.id.doneButton);
        Button cancelButton = (Button) getView().findViewById(R.id.cancelButton);
        answerText = (EditText) getView().findViewById(R.id.answerText);
        TextView questionText = (TextView) getView().findViewById(R.id.questionText);

        questionText.setText(preferences.getString(GlobalVariables.SECURITY_QUESTION_PREF, "Security Question"));

        preferences.edit().putString(GlobalVariables.SECURITY_ANSWER_PREF, "abc");

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (preferences.getString(GlobalVariables.SECURITY_ANSWER_PREF, null).equals(answerText.getText().toString())) {
                    Toast.makeText(getActivity(), "Password: " + preferences.getString(GlobalVariables.Password, null), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Incorrect answer.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PasswordFragment passwordFragment = PasswordFragment.newInstance();
                getActivity().findViewById(R.id.passwordFragmentContainer).setVisibility(View.VISIBLE);
                getFragmentManager().beginTransaction().replace(R.id.passwordFragmentContainer,
                        passwordFragment, SetPasswordFragment.TAG).commit();
            }
        });
    }
}
