package fullsail.bot.com.sightbot.LessonResultsScreen;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import fullsail.bot.com.sightbot.CustomClasses.LessonProgressCustomClass;
import fullsail.bot.com.sightbot.GlobalVariables;
import fullsail.bot.com.sightbot.LessonResultsScreen.Fragments.ResultsFragment;
import fullsail.bot.com.sightbot.LessonResultsScreen.Fragments.SightBotFragment;
import fullsail.bot.com.sightbot.R;

public class LessonResultsActivity extends AppCompatActivity {
    LessonProgressCustomClass lessonProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson_results);

        lessonProgress = (LessonProgressCustomClass) getIntent().getSerializableExtra(GlobalVariables.EXTRA_LESSON_PROGRESS);
        lessonProgress.getKid().setNextLesson(lessonProgress.getPercentage());
        //Send Email:
        Intent intent = new Intent(this, SendProgressEmailService.class);
        intent.putExtra(GlobalVariables.EXTRA_LESSON_PROGRESS, lessonProgress);
        startService(intent);

        loadSightBotFragment();
        ResultsFragment resultsFragment = ResultsFragment.newInstance(lessonProgress);
        getFragmentManager().beginTransaction().replace(R.id.results_container, resultsFragment).commit();
    }

    private void loadSightBotFragment(){
        SightBotFragment frag = SightBotFragment.newInstance();
        getFragmentManager().beginTransaction().replace(R.id.your_video_view, frag).commit();
    }

}
