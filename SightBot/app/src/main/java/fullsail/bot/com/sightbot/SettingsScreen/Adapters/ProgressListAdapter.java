package fullsail.bot.com.sightbot.SettingsScreen.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import fullsail.bot.com.sightbot.CustomClasses.KidCustomClass;
import fullsail.bot.com.sightbot.R;

/**
 * Created by snipetysnipes on 3/12/16.
 */
public class ProgressListAdapter extends BaseAdapter {

    public static final String TAG = "Articles.TAG";
    public static final int ID_CONSTANT = 0x0100000;

    Context mContext;
    int quarter;
    KidCustomClass kid;

    ArrayList<Double> quarterPercentages;

    ArrayList<KidCustomClass.WordProgress> wordProgress;

    public ProgressListAdapter(Context mContext, KidCustomClass kid, int quarter) {
        this.mContext = mContext;
        this.kid = kid;
        this.quarter = quarter;

        if (quarter < 4) {
            wordProgress = kid.getChildProgress().get(quarter);
        } else {
            quarterPercentages = new ArrayList<>();
            for (int i = 0; i < 4; i++) {
                 quarterPercentages.add(kid.getQuarterPercentage(i));
            }
        }
    }

    @Override
    public int getCount() {

        if (wordProgress != null) {
            return wordProgress.size();
        } else if (quarterPercentages != null) {
            return quarterPercentages.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int i) {

        if (wordProgress != null && i < wordProgress.size() && i >= 0) {
            return wordProgress.get(i);
        } else if (quarterPercentages!= null && i < quarterPercentages.size() && i >= 0) {
            return quarterPercentages.get(i);
        } else {
            Log.i(TAG, "Error handling!");
            return null;
        }

    }

    @Override
    public long getItemId(int i) {

        if (wordProgress != null) {
            return ID_CONSTANT + i;
        } else if (quarterPercentages != null) {
            return ID_CONSTANT + i;
        } else {
            Log.i(TAG, "Returned 0");
            return 0;
        }
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.progress_list_itemlayout, parent, false);
        }

        TextView listWord = (TextView) convertView.findViewById(R.id.listWord);
        TextView listAttempts = (TextView) convertView.findViewById(R.id.listAttempts);
        TextView listSuccesses = (TextView) convertView.findViewById(R.id.listSuccesses);

        if (wordProgress != null) {
            listWord.setText(String.valueOf(wordProgress.get(i).getWord()));
            listAttempts.setText(String.valueOf(wordProgress.get(i).getNumberOfTimesSeen()));
            listSuccesses.setText(String.valueOf(wordProgress.get(i).getNumberOfTimesCorrect()));
        } else {
            switch (i) {
                case 0:
                    listWord.setText("1st Quarter");
                    break;
                case 1:
                    listWord.setText("2nd Quarter");
                    break;
                case 2:
                    listWord.setText("3rd Quarter");
                    break;
                case 3:
                    listWord.setText("4th Quarter");
                    break;
            }

            listSuccesses.setText(String.valueOf(quarterPercentages.get(i)) + "%");
        }

        return convertView;
    }
}
