package fullsail.bot.com.sightbot;

public class GlobalVariables {

    //Sight Words
    //First Quarter
    public static final String[] wordsFromFirstQuarter = {"the","I","a","to","is","my","go","me","like","on","in","so",
            "we","it","and","up","at","see","he","do","you","an","can","no","am"};
    public static final String[] wordsFromSecondQuarter = {"went", "are", "this", "look", "for", "get", "come", "that",
            "play", "was", "had", "they", "will", "too", "all", "be", "as", "one", "by", "what",
            "did", "has", "her", "him", "with"};
    public static final String[] wordsFromThirdQuarter = {"eat", "if", "but", "made", "or", "not", "said", "out", "now",
            "of", "put", "ran", "sat", "read", "boy", "she", "sit", "then", "his", "more", "us",
            "yes", "saw", "girl", "how"};
    public static final String[] wordsFromFourthQuarter = {"when", "your", "about", "from", "than", "away", "them",
            "came", "big", "many", "after", "who", "back", "find", "because", "very", "could",
            "have", "make", "number", "into", "there", "were", "people", "just"};

    public static final String Password = "PASSWORD";
    public static final String Email = "EMAIL";

    public static final String BROADCAST_IMAGE_TAKEN = "BROADCAST_IMAGE_TAKEN";
    public static final String BROADCAST_SKIP_INSTRUCTIONS_ANIMATION = "BROADCAST_SKIP_INSTRUCTIONS_ANIMATION";
    public static final String CHILDNAME_EXTRA = "CHILDNAME_EXTRA";
    public static final String CHILD_EXTRA = "CHILD_EXTRA";
    public static final String ACTION_TIMES_UP = "fullsail.bot.com.sightbot_ACTION_TIMES_UP";
    public static final String ACTION_PAUSE_TIMER = "fullsail.bot.com.sightbot_ACTION_PAUSE_TIMER";
    public static final String EXTRA_LESSON_PROGRESS = "EXTRA_LESSON_PROGRESS";
    public static final String EXTRA_IMAGE_LOCATION_AS_STRING = "EXTRA_IMAGE_LOCATION_AS_STRING";

    public static final String SECURITY_QUESTION_PREF = "SECURITY_QUESTION_PREF";
    public static final String SECURITY_ANSWER_PREF = "SECURITY_ANSWER_PREF";
    public static final int ANIMATION_CORRECT = 0;
    public static final int ANIMATION_INCORRECT = 1;
    public static final int ANIMATION_INSTRUCTIONS = 2;


}
