package fullsail.bot.com.sightbot.LessonResultsScreen.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

import fullsail.bot.com.sightbot.CustomClasses.LessonProgressCustomClass;
import fullsail.bot.com.sightbot.DataHelper;
import fullsail.bot.com.sightbot.LessonResultsScreen.Adapters.TrophyGridAdapter;
import fullsail.bot.com.sightbot.R;

/**
 * Created by snipetysnipes on 3/21/16.
 */
public class ResultsFragment extends Fragment implements TextToSpeech.OnInitListener {

    private static final String TAG = "ResultsFragment.TAG";

    TextToSpeech tts;
    ArrayList<String> newTrophyNames = new ArrayList<>();
    ArrayList<String> oldTrophyNames = new ArrayList<>();

    public ResultsFragment() {
    }

    GridView oldTrophiesGrid;
    GridView newTrophiesGrid;

    public static ResultsFragment newInstance(LessonProgressCustomClass progress) {

        ResultsFragment frag = new ResultsFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable("progress", progress);
        frag.setArguments(bundle);

        return frag;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tts = new TextToSpeech(getActivity(), this);
        tts.setPitch(0.8f);
        tts.setSpeechRate(0.8f);

        String[] words = {"Awesome!", "Nice Job!", "Fantastic!", "Sightastic!", "Incredible!", "Spectacular!", "Superb!", "Superduper!", "You're So Smart!"};

        Random random = new Random();

        TextView textViewResult = (TextView) view.findViewById(R.id.textViewResult);
        int ranNum = random.nextInt(words.length - 1);
        textViewResult.setText(words[ranNum]);

        LessonProgressCustomClass lessonProgress = (LessonProgressCustomClass) getArguments().getSerializable("progress");

        ((RatingBar) view.findViewById(R.id.lesson_rating)).setMax(100);
        ((RatingBar) view.findViewById(R.id.lesson_rating)).setProgress(lessonProgress.getPercentage());
        view.findViewById(R.id.lesson_rating).setEnabled(false);

        newTrophiesGrid = (GridView) view.findViewById(R.id.new_trophies_grid);
        oldTrophiesGrid = (GridView) view.findViewById(R.id.old_trophies_grid);

        int quarter = lessonProgress.getKid().getCurrentQuarter();
        int week = lessonProgress.getKid().getCurrentWeek();

        oldTrophyNames = DataHelper.getAllTrophies(getActivity());

        if (!DataHelper.saveTrophy(getActivity(), "Quarter " + quarter + ", Week " + week)) {
            newTrophyNames.add("Quarter " + quarter + ", Week " + week);
        }

        final TrophyGridAdapter trophyGridAdapter = new TrophyGridAdapter(getActivity(), newTrophyNames);
        newTrophiesGrid.setAdapter(trophyGridAdapter);

        final TrophyGridAdapter trophyGridAdapterOld = new TrophyGridAdapter(getActivity(), oldTrophyNames);
        oldTrophiesGrid.setAdapter(trophyGridAdapterOld);

        newTrophiesGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                speak(newTrophyNames.get(position));
                moveTrophy(newTrophyNames.get(position), position);
            }
        });

        oldTrophiesGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                speak(oldTrophyNames.get(position));
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.results_frag, container, false);
    }

    //Text to speech onInitListener
    @Override
    public void onInit(int status) {

    }

    private void speak (String trophyName) {
        if (tts != null) {
            tts.speak("You earned the " + trophyName + " badge!", TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    private void moveTrophy (String trophyName, int position) {
        //SharedPreferences sharedPreferences = getActivity().getSharedPreferences("OLD_TROPHIES", Context.MODE_PRIVATE);

        newTrophyNames.remove(position);
        oldTrophyNames.add(trophyName);

        final TrophyGridAdapter trophyGridAdapterNew = new TrophyGridAdapter(getActivity(), newTrophyNames);
        newTrophiesGrid.setAdapter(trophyGridAdapterNew);

        final TrophyGridAdapter trophyGridAdapterOld = new TrophyGridAdapter(getActivity(), oldTrophyNames);
        oldTrophiesGrid.setAdapter(trophyGridAdapterOld);

        //SharedPreferences sharedPreferences = getActivity().getSharedPreferences("OLD_TROPHIES", Context.MODE_PRIVATE);
        //sharedPreferences.edit().putString("OLD_TROPHIES", trophyName).apply();
    }
}
