package fullsail.bot.com.sightbot.LessonResultsScreen.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

import fullsail.bot.com.sightbot.R;

/**
 * Created by loganmckinzie on 3/21/16.
 */
public class TrophyGridAdapter extends BaseAdapter {

    public static final String TAG = "TrophyGridAdapter.TAG";
    public static final int ID_CONSTANT = 0x01000000;

    Context mContext;
    ArrayList<String> trophyNames;

    public TrophyGridAdapter(Context mContext, ArrayList<String> trophyNames) {
        this.mContext = mContext;
        this.trophyNames = trophyNames;
    }

    @Override
    public int getCount() {
        if (trophyNames != null) {
            return trophyNames.size();
        } else {
            return 0;
        }
    }

    @Override
    public String getItem(int position) {
        if (trophyNames != null && position < trophyNames.size() && position >= 0) {
            return trophyNames.get(position);
        } else {
            Log.i(TAG, "Error handling! Something wrong with getItem");
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        if (trophyNames != null) {
            return ID_CONSTANT + position;
        } else {
            Log.i(TAG, "Returned 0 because there's no collection.");
            return 0;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.trophy_image, parent, false);

            ((ImageView)convertView.findViewById(R.id.trophyImageView)).setImageBitmap((getTrophy(trophyNames.get(position))));
        }

        return convertView;
    }

    public Bitmap getTrophy(String trophyName) {

        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = 4;

        switch (trophyName) {
            case "Quarter 1, Week 1":
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy1_1, opts);
            case "Quarter 1, Week 2":
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy1_2, opts);
            case "Quarter 1, Week 3":
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy1_3, opts);
            case "Quarter 1, Week 4":
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy1_4, opts);
            case "Quarter 1, Week 5":
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy1_5, opts);
            case "Quarter 1, Week 9":
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy1_5, opts);

            case "Quarter 2, Week 1":
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy2_1, opts);
            case "Quarter 2, Week 2":
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy2_2, opts);
            case "Quarter 2, Week 3":
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy2_3, opts);
            case "Quarter 2, Week 4":
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy2_4, opts);
            case "Quarter 2, Week 5":
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy2_5, opts);
            case "Quarter 2, Week 9":
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy2_5, opts);

            case "Quarter 3, Week 1":
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy3_1, opts);
            case "Quarter 3, Week 2":
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy3_2, opts);
            case "Quarter 3, Week 3":
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy3_3, opts);
            case "Quarter 3, Week 4":
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy3_4, opts);
            case "Quarter 3, Week 5":
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy3_5, opts);
            case "Quarter 3, Week 9":
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy3_5, opts);

            case "Quarter 4, Week 1":
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy4_1, opts);
            case "Quarter 4, Week 2":
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy4_2, opts);
            case "Quarter 4, Week 3":
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy4_3, opts);
            case "Quarter 4, Week 4":
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy4_4, opts);
            case "Quarter 4, Week 5":
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy4_5, opts);
            case "Quarter 4, Week 9":
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy4_5, opts);

            default:
                return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.trophy1_1, opts);
        }
    }
}
