package fullsail.bot.com.sightbot.SettingsScreen.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import fullsail.bot.com.sightbot.DataHelper;
import fullsail.bot.com.sightbot.GlobalVariables;
import fullsail.bot.com.sightbot.R;

/**
 * Created by davidfermer on 3/11/16.
 */
public class ChangeEmailFragment extends Fragment implements View.OnClickListener{

    public ChangeEmailFragment() {
    }

    public static ChangeEmailFragment newInstance(){
        return new ChangeEmailFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_email_change, container, false);
        v.findViewById(R.id.cancel_resetEmail).setOnClickListener(this);
        v.findViewById(R.id.change_Email_done_botton).setOnClickListener(this);
        return v;
    }


    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.cancel_resetEmail){

            //The change email address was cancelled reloading first kid's detail fragment
            reloadFirstKid();
        } else if(v.getId() == R.id.change_Email_done_botton) {

            //Checking to ensure valid email address
            EditText emailAddressTextField = (EditText)getActivity().findViewById(R.id.new_email_address);
            boolean emailIsValid = Patterns.EMAIL_ADDRESS.matcher(emailAddressTextField.getText()).matches();
            if(!emailIsValid) {

                //Email address is invalid
                Vibrator vibrator = (Vibrator)getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                Toast.makeText(getActivity(), "Please enter a valid Email Address", Toast.LENGTH_SHORT).show();
                vibrator.vibrate(500);
            } else {

                //Email address is valid
                SharedPreferences preferences = getActivity().getSharedPreferences(GlobalVariables.Email, Context.MODE_PRIVATE);
                preferences.edit().putString(GlobalVariables.Email, emailAddressTextField.getText().toString()).apply();
                reloadFirstKid();
            }
        }
    }

    private void reloadFirstKid(){
        SettingsDetailFragment fragment = SettingsDetailFragment.newInstance(DataHelper.getAllKids(getActivity()).get(0));
        getFragmentManager().beginTransaction().replace(R.id.details, fragment).commit();
    }
}
