package fullsail.bot.com.sightbot.LessonScreen.Fragments;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import fullsail.bot.com.sightbot.GlobalVariables;
import fullsail.bot.com.sightbot.R;


public class SightBotAnimationFragment extends Fragment {

    public static final String TAG = "SightBotAnimationFragment";
    public static final String ANIMATION_TYPE = "ANIMATION_TYPE";
    private static final int MAIN_REQUEST_CODE = 0x00011;
    AnimationFinishedInterface finishedInterface;
    VideoView videoHolder;
    StopAnimationReceiver receiver;
    Uri videoUri;
    boolean animationFinished = false;
    int positionInTotalAnimationWhenPaused;

    public static SightBotAnimationFragment newIntance(int animationType) {
        SightBotAnimationFragment fragment = new SightBotAnimationFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ANIMATION_TYPE, animationType);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        finishedInterface = (AnimationFinishedInterface)getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        finishedInterface = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(GlobalVariables.BROADCAST_SKIP_INSTRUCTIONS_ANIMATION);
        receiver = new StopAnimationReceiver();
        getActivity().registerReceiver(receiver, filter);
        if(!animationFinished) {
            videoHolder.start();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(receiver);
        if (!animationFinished) {
            videoHolder.stopPlayback();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        int animation = getArguments().getInt(ANIMATION_TYPE);
        final View view = inflater.inflate(R.layout.fragment_lesson_sight_bot, container, false);
        videoHolder = (VideoView) view.findViewById(R.id.sightBotVideoView);
        switch (animation) {
            case GlobalVariables.ANIMATION_INSTRUCTIONS:
                videoUri = Uri.parse("android.resource://" + getActivity().getPackageName() + "/"
                        + R.raw.sight_bot_instructions);
                videoHolder.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        animationFinished = true;
                        finishedInterface.animationDidFinish();
                    }
                });
                break;
            case GlobalVariables.ANIMATION_CORRECT:
                videoUri = Uri.parse("android.resource://" + getActivity().getPackageName() + "/"
                        + R.raw.correct);
                break;
            case GlobalVariables.ANIMATION_INCORRECT:
                videoUri = Uri.parse("android.resource://" + getActivity().getPackageName() + "/"
                        + R.raw.incorrect);

        }


        videoHolder.setVideoURI(videoUri);
        videoHolder.start();

        return view;
    }

    public interface AnimationFinishedInterface{
        void animationDidFinish();
    }

    class StopAnimationReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            videoHolder.seekTo(videoHolder.getDuration() - 1);
        }
    }
}
