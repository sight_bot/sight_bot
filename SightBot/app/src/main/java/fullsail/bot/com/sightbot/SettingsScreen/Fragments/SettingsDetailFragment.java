package fullsail.bot.com.sightbot.SettingsScreen.Fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import fullsail.bot.com.sightbot.CustomClasses.ImageCustomClass;
import fullsail.bot.com.sightbot.SettingsScreen.Adapters.ProgressListAdapter;
import fullsail.bot.com.sightbot.CustomClasses.KidCustomClass;
import fullsail.bot.com.sightbot.DataHelper;
import fullsail.bot.com.sightbot.R;

/**
 * Created by snipetysnipes on 3/7/16.
 */
public class SettingsDetailFragment extends Fragment implements AlertDialog.OnClickListener, View.OnClickListener {

    public static final String TAG = "LoginFragment.TAG";
    Button tab1;
    Button tab2;
    Button tab3;
    Button tab4;
    Button tab5;
    ArrayList<Button> tabButtons = new ArrayList<>();
    ListView progressList;
    ImageView detailImage;

    ProgressListAdapter progressListAdapter;
    KidCustomClass kids;

    public static SettingsDetailFragment newInstance(KidCustomClass childBeingEdited){
        SettingsDetailFragment frag = new SettingsDetailFragment();

            Bundle bundle = new Bundle();
            bundle.putSerializable("childBeingEdited", childBeingEdited);
            frag.setArguments(bundle);

        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_settings_detail, container, false);

        TextView name = (TextView) view.findViewById(R.id.nameDetail);
        TextView gender = (TextView) view.findViewById(R.id.genderDetail);
        TextView grade = (TextView) view.findViewById(R.id.gradeDetail);
        TextView teacher = (TextView) view.findViewById(R.id.teacherDetail);
        TextView teacherEmail = (TextView) view.findViewById(R.id.teacherEmailDetail);
        TextView parentEmail = (TextView) view.findViewById(R.id.parentEmailDetail);
        TextView week = (TextView) view.findViewById(R.id.currentWeekDetail);
        TextView quarter = (TextView) view.findViewById(R.id.currentQuarterDetail);
        detailImage = (ImageView) view.findViewById(R.id.detailImage);
        progressList = (ListView) view.findViewById(R.id.progressList);
        view.findViewById(R.id.erase_child_progress).setOnClickListener(this);

        //Buttons
        tab1 = (Button) view.findViewById(R.id.tab1);
        tab2 = (Button) view.findViewById(R.id.tab2);
        tab3 = (Button) view.findViewById(R.id.tab3);
        tab4 = (Button) view.findViewById(R.id.tab4);
        tab5 = (Button) view.findViewById(R.id.tab5);

        tabButtons.add(tab1);
        tabButtons.add(tab2);
        tabButtons.add(tab3);
        tabButtons.add(tab4);
        tabButtons.add(tab5);

        for (int i = 0; i < tabButtons.size(); i++){
            tabButtons.get(i).setOnClickListener(this);
        }

        if(getArguments().getSerializable("childBeingEdited") != null){

            Bundle args = getArguments();
            kids = (KidCustomClass) args.getSerializable("childBeingEdited");

//            detailImage.setImageBitmap(DataHelper.getImage(kids.getChildName()));
            ImageCustomClass imageCustomClass = DataHelper.getSpecificImage(getActivity(), kids.getChildName());
            detailImage.setImageBitmap(imageCustomClass.getChildImage());
            
            name.setText(kids.getChildName());
            gender.setText(kids.getGender());
            grade.setText(kids.getGrade());
            teacher.setText(kids.getTeacherName());
            teacherEmail.setText(kids.getTeacherEmail());
            parentEmail.setText(String.valueOf(kids.getIsParentEmailUtilized()));
            week.setText(String.valueOf(kids.getCurrentWeek()));
            quarter.setText(String.valueOf(kids.getCurrentQuarter()));
        }


        return view;
    }

    @Override
    public void onClick(View v) {

        for (int i = 0; i < tabButtons.size(); i++) {
            if (v.getId() == tabButtons.get(i).getId()) {
                tabButtons.get(i).setAlpha(1.0f);
            } else if (v.getId() != tabButtons.get(i).getId()){
                tabButtons.get(i).setAlpha(0.5f);
            }
        }

        switch (v.getId()) {
            case R.id.tab1:
                ((TextView)getActivity().findViewById(R.id.attemptsLabel)).setText("#Attempts");
                ((TextView)getActivity().findViewById(R.id.successesLabel)).setText("#Correct");
                kids = (KidCustomClass)getArguments().getSerializable("childBeingEdited");
                progressListAdapter = new ProgressListAdapter(getActivity(), kids, 0);
                break;
            case R.id.tab2:
                ((TextView)getActivity().findViewById(R.id.attemptsLabel)).setText("#Attempts");
                ((TextView)getActivity().findViewById(R.id.successesLabel)).setText("#Correct");
                kids = (KidCustomClass)getArguments().getSerializable("childBeingEdited");
                progressListAdapter = new ProgressListAdapter(getActivity(), kids, 1);
                break;
            case R.id.tab3:
                ((TextView)getActivity().findViewById(R.id.attemptsLabel)).setText("#Attempts");
                ((TextView)getActivity().findViewById(R.id.successesLabel)).setText("#Correct");
                kids = (KidCustomClass)getArguments().getSerializable("childBeingEdited");
                progressListAdapter = new ProgressListAdapter(getActivity(), kids, 2);
                break;
            case R.id.tab4:
                ((TextView)getActivity().findViewById(R.id.attemptsLabel)).setText("#Attempts");
                ((TextView)getActivity().findViewById(R.id.successesLabel)).setText("#Correct");
                kids = (KidCustomClass)getArguments().getSerializable("childBeingEdited");
                progressListAdapter = new ProgressListAdapter(getActivity(), kids, 3);
                break;
            case R.id.tab5:
                ((TextView)getActivity().findViewById(R.id.attemptsLabel)).setText("Quarter");
                ((TextView)getActivity().findViewById(R.id.successesLabel)).setText("Percentage Correct");
                kids = (KidCustomClass)getArguments().getSerializable("childBeingEdited");
                progressListAdapter = new ProgressListAdapter(getActivity(), kids, 4);
                break;
            case R.id.erase_child_progress:
                AlertDialog alert = new AlertDialog.Builder(getActivity())
                        .setTitle("Erase All Progress?")
                        .setMessage("Are you sure you want to erase all of " +
                                ((KidCustomClass) getArguments().getSerializable("childBeingEdited")).getChildName() + " progress?")
                        .setPositiveButton("Erase", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ((KidCustomClass)getArguments().getSerializable("childBeingEdited")).eraseChildsProgress();
                                SettingsDetailFragment fragment = newInstance((KidCustomClass)getArguments().getSerializable("childBeingEdited"));
                                getFragmentManager().beginTransaction().replace(R.id.details, fragment).commit();
                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .create();
                alert.show();
            default:
                break;
        }

        progressList.setAdapter(progressListAdapter);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        kids =(KidCustomClass)getArguments().getSerializable("childBeingEdited");
        progressListAdapter = new ProgressListAdapter(getActivity(), kids, 0);
        progressList.setAdapter(progressListAdapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.settings_detail_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.edit_child){
            SettingsEditDetailFragment fragment = SettingsEditDetailFragment.newInstance((KidCustomClass)getArguments().getSerializable("childBeingEdited"));
            getFragmentManager().beginTransaction().replace(R.id.details, fragment).commit();
            return true;

        } else if(item.getItemId() == R.id.add_child){
            SettingsEditDetailFragment fragment = SettingsEditDetailFragment.newInstance(null);
            getFragmentManager().beginTransaction().replace(R.id.details, fragment).commit();
            return true;

        } else if(item.getItemId() == R.id.change_parent_email){
            ChangeEmailFragment fragment = ChangeEmailFragment.newInstance();
            getFragmentManager().beginTransaction().replace(R.id.details, fragment).commit();
            return true;

        } else if(item.getItemId() == R.id.change_password){
            ChangePasswordFragment fragment = ChangePasswordFragment.newInstance();
            getFragmentManager().beginTransaction().replace(R.id.details,fragment).commit();
            return true;

        } else if (item.getItemId() == R.id.delete_child){
           AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                   .setTitle("Delete Child Account")
                   .setMessage("Are you sure you want to delete " +
                           ((KidCustomClass) getArguments().getSerializable("childBeingEdited"))
                                   .getChildName()
                           + "'s account?")
                   .setPositiveButton("Delete", this)
                   .setNegativeButton("Cancel", null);
            AlertDialog alert = builder.create();
            alert.show();
            return true;
        }

        return false;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == -1){

            //The user wants to delete the child's account
            DataHelper.deleteKid(getActivity(),((KidCustomClass)getArguments().getSerializable("childBeingEdited")).getChildName());
            if(DataHelper.getAllKids(getActivity()).size() != 0){
                SettingsDetailFragment fragment = SettingsDetailFragment.newInstance(DataHelper.getAllKids(getActivity()).get(0));
                getFragmentManager().beginTransaction().replace(R.id.details,fragment).commit();
            } else {
                SettingsEditDetailFragment fragment = SettingsEditDetailFragment.newInstance(null);
                getFragmentManager().beginTransaction().replace(R.id.details, fragment).commit();
            }

            SettingsChildFragment fragment = SettingsChildFragment.newInstance(DataHelper.getAllKids(getActivity()));
            getFragmentManager().beginTransaction().replace(R.id.children, fragment).commit();
        }
    }
}
