package fullsail.bot.com.sightbot.LaunchScreen.Fragments;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;
import fullsail.bot.com.sightbot.LoginScreen.LoginActivity;
import fullsail.bot.com.sightbot.R;

public class LaunchScreenFragment extends Fragment {

    public static final String TAG = "LaunchScreenFragment";
    private static final int MAIN_REQUEST_CODE = 0x00000;

    public static LaunchScreenFragment newIntance() {
        return new LaunchScreenFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_launch_screen, container, false);
        final VideoView videoHolder = (VideoView) view.findViewById(R.id.videoView);

        Uri videoUri = Uri.parse("android.resource://" + getActivity().getPackageName() + "/"
                + R.raw.sight_bot_launch);
        videoHolder.setVideoURI(videoUri);
        videoHolder.start();
        videoHolder.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                getActivity().finish();
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivityForResult(intent, MAIN_REQUEST_CODE);
            }
        });

        return view;
    }

}
