package fullsail.bot.com.sightbot;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import fullsail.bot.com.sightbot.CustomClasses.ImageCustomClass;
import fullsail.bot.com.sightbot.CustomClasses.KidCustomClass;


public class DataHelper {

    private static final String kidsFileName = "kidsFile";
    private static final String imagesFileName = "imagesFile";
    private static final String trophiesFileName = "trophyFile";

    //Children Data helper
    private static void saveAllKids(Context context, ArrayList<KidCustomClass> kids){
        try {
            FileOutputStream fos = context.openFileOutput(kidsFileName, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(kids);
            oos.close();
        } catch (java.io.IOException e){
            e.printStackTrace();
        }
    }

    public static ArrayList<KidCustomClass> getAllKids(Context context){
        if(!new File(context.getFilesDir()+"/"+kidsFileName).exists()){
            return new ArrayList<>();
        }
        try{
            FileInputStream fis = context.openFileInput(kidsFileName);
            ObjectInputStream ois = new ObjectInputStream(fis);
            return (ArrayList<KidCustomClass>) ois.readObject();
        } catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    //If this function returns true that means the child's name is already in use
    // and they must use a different child name
    public static boolean saveNewKid(Context context, KidCustomClass kid){
        ArrayList<KidCustomClass> kids = getAllKids(context);
        for(KidCustomClass child : kids){
            if(kid.getChildName().equals(child.getChildName())){
                return true;
            }
        }
        kids.add(kid);
        saveAllKids(context, kids);
        return false;
    }

    public static KidCustomClass getSpecificKid(Context context, String childName){
        ArrayList<KidCustomClass> kids = getAllKids(context);
        for(KidCustomClass kid : kids){
            if(kid.checkKidNameEquals(childName)){
                return kid;
            }
        }
        return null;
    }

    public static void deleteKid(Context context, String childName){
        ArrayList<KidCustomClass> kids = getAllKids(context);
        for(KidCustomClass kid : kids){
            if(kid.checkKidNameEquals(childName)){
                kids.remove(kid);
                break;
            }
        }
        saveAllKids(context, kids);
    }

    public static void updateKid(Context context, KidCustomClass kid){
        ArrayList<KidCustomClass> children = getAllKids(context);
        for (int i = 0; i < children.size(); i++){
            if(children.get(i).checkKidNameEquals(kid.getChildName())){
                children.remove(i);
                children.add(i,kid);
            }
        }
        saveAllKids(context, children);
    }

    //Image Shit -
    private static void saveAllImages(Context context, ArrayList<ImageCustomClass> images){
        try {
            FileOutputStream fos = context.openFileOutput(imagesFileName, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(images);
            oos.close();
        } catch (java.io.IOException e){
            e.printStackTrace();
        }
    }

    public static ArrayList<ImageCustomClass> getAllImages(Context context){
        if(!new File(context.getFilesDir()+"/"+imagesFileName).exists()){
            return new ArrayList<>();
        }
        try{
            FileInputStream fis = context.openFileInput(imagesFileName);
            ObjectInputStream ois = new ObjectInputStream(fis);
            return (ArrayList<ImageCustomClass>) ois.readObject();
        } catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    //If this function returns true that means the child's name is already in use
    // and they must use a different child name
    public static boolean saveNewImage(Context context, ImageCustomClass imageCustomClass){
        ArrayList<ImageCustomClass> images = getAllImages(context);
        for(ImageCustomClass i : images){
            if(imageCustomClass.getChildName().equals(i.getChildName())){
                return true;
            }
        }
        images.add(imageCustomClass);
        saveAllImages(context, images);
        return false;
    }

    public static ImageCustomClass getSpecificImage(Context context, String childName){
        ArrayList<ImageCustomClass> images = getAllImages(context);
        for(ImageCustomClass i : images){
            if(i.getChildName().equals(childName)){
                return i;
            }
        }
        return null;
    }

    public static void updateImage(Context context, ImageCustomClass image){
        ArrayList<ImageCustomClass> images = getAllImages(context);
        for (int i = 0; i < images.size(); i++){
            if(images.get(i).getChildName().equals(image.getChildName())){
                images.remove(i);
                images.add(i,image);
            }
        }
        saveAllImages(context,images);
    }

    public static ArrayList<String> getAllTrophies (Context context) {

        if(!new File(context.getFilesDir()+"/"+trophiesFileName).exists()){
            return new ArrayList<>();
        }

        try{
            FileInputStream fis = context.openFileInput(trophiesFileName);
            ObjectInputStream ois = new ObjectInputStream(fis);
            return (ArrayList<String>) ois.readObject();
        } catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public static boolean saveTrophy (Context context, String trophyName) {

        ArrayList<String> trophies = getAllTrophies(context);

        for(String s : trophies){
            if(s.equals(trophyName)) {
                return true;
            }
        }
        trophies.add(trophyName);
        try {
            FileOutputStream fos = context.openFileOutput(trophiesFileName, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(trophies);
            oos.close();
        } catch (java.io.IOException e){
            e.printStackTrace();
        }

        return false;
    }
}
