package fullsail.bot.com.sightbot.LoginScreen.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Vibrator;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import fullsail.bot.com.sightbot.SettingsScreen.SettingsActivity;
import fullsail.bot.com.sightbot.GlobalVariables;
import fullsail.bot.com.sightbot.R;

/**
 * Created by davidfermer on 3/7/16.
 */
public class PasswordFragment extends Fragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    public static final String TAG = "PasswordFrag.TAG";

    public PasswordFragment() {
    }

    public static PasswordFragment newInstance() {
        return new PasswordFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View passwordView = inflater.inflate(R.layout.dialog_view_enter_password, container, false);

        passwordView.findViewById(R.id.forgot_Password_Button).setOnClickListener(this);
        passwordView.findViewById(R.id.cancel_parentLogin).setOnClickListener(this);
        passwordView.findViewById(R.id.login_Button).setOnClickListener(this);
        ((CheckBox)passwordView.findViewById(R.id.checkboxShowPassword)).setOnCheckedChangeListener(this);

        return passwordView;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.forgot_Password_Button) {
            passwordForgotten();
        } else if (v.getId() == R.id.login_Button) {
            SharedPreferences preferences = getActivity().getSharedPreferences(
                    GlobalVariables.Password, Context.MODE_PRIVATE);
            String password = preferences.getString(GlobalVariables.Password, null);
            //Check to see if the password is correct
            if(((EditText)(getActivity().findViewById(R.id.dialog_view_password_field))).getText()
                    .toString().equals(password)){
                //The password was correct
                getFragmentManager().beginTransaction().remove(this).commit();
                Intent goToSettingsActivityIntent = new Intent(getActivity(), SettingsActivity.class);
                getActivity().findViewById(R.id.passwordFragmentContainer).setVisibility(View.GONE);
                startActivity(goToSettingsActivityIntent);
            } else {
                //the password was incorrect - Check to see if the incorrectPasswordLabel was already visible
                Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                if(getActivity().findViewById(R.id.incorrectPasswordLabel).getVisibility() == View.VISIBLE){
                    //It was already visible
                    Animation animShake = AnimationUtils.loadAnimation(getActivity(),
                            R.anim.slide);
                    getActivity().findViewById(R.id.incorrectPasswordLabel).startAnimation(animShake);
                    //TODO - Dave add some animation to shake the incorrect Password label - need Tim's assistance here
                    vibrator.vibrate(500);
                } else {
                    getActivity().findViewById(R.id.incorrectPasswordLabel).setVisibility(View.VISIBLE);
                    Animation animShake = AnimationUtils.loadAnimation(getActivity(),
                            R.anim.slide);
                    getActivity().findViewById(R.id.incorrectPasswordLabel).startAnimation(animShake);
                    vibrator.vibrate(500);
                }
            }
        } else if (v.getId() == R.id.cancel_parentLogin){
            ((EditText)getActivity().findViewById(R.id.dialog_view_password_field)).setText("");
            getFragmentManager().beginTransaction().remove(this).commit();
            getActivity().findViewById(R.id.passwordFragmentContainer).setVisibility(View.GONE);
        }
    }

    public void passwordForgotten() {
        PasswordSecurityQuestionFragment frag = PasswordSecurityQuestionFragment.newInstance();
        getActivity().findViewById(R.id.passwordFragmentContainer).setVisibility(View.VISIBLE);
        getFragmentManager().beginTransaction().replace(R.id.passwordFragmentContainer, frag).commit();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        //If checkbox is checked the password will be shown else it will not be shown
        if(isChecked){
            ((EditText)getActivity().findViewById(R.id.dialog_view_password_field))
                    .setInputType(InputType.TYPE_CLASS_TEXT);
        } else {
            ((EditText)getActivity().findViewById(R.id.dialog_view_password_field))
                    .setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
    }
}
