package fullsail.bot.com.sightbot.LoginScreen.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import fullsail.bot.com.sightbot.R;

/**
 * Created by snipetysnipes on 3/7/16.
 */
public class CreateAccFragment extends Fragment {
    public static final String TAG = "CreateAccFragment.TAG";

    public static CreateAccFragment newInstance(){
        return new CreateAccFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_login, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.settings_action, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        PasswordFragment passFrag = PasswordFragment.newInstance();
        getActivity().findViewById(R.id.passwordFragmentContainer).setVisibility(View.VISIBLE);
        getFragmentManager().beginTransaction().replace(R.id.passwordFragmentContainer, passFrag, PasswordFragment.TAG).commit();
        return true;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }
}
