package fullsail.bot.com.sightbot.LoginScreen.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import fullsail.bot.com.sightbot.LessonScreen.LessonActivity;
import fullsail.bot.com.sightbot.R;

/**
 * Created by davidfermer on 3/7/16.
 */
public class LoginFragment extends Fragment{

    public static final String TAG = "LoginFragment.TAG";

    public LoginFragment() {
    }

    public static LoginFragment newInstance(){
        return new LoginFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_login, container, false);
        HorizontalScrollView scrollView = (HorizontalScrollView) view.findViewById(R.id.horizontalScrollView);
        scrollView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LessonActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.settings_action, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        SetPasswordFragment setPasswordFragment = SetPasswordFragment.newInstance();
        getActivity().findViewById(R.id.hint).setVisibility(View.GONE);
        getActivity().findViewById(R.id.imageView2).setVisibility(View.GONE);
        getActivity().findViewById(R.id.passwordFragmentContainer).setVisibility(View.VISIBLE);
        getFragmentManager().beginTransaction().replace(R.id.passwordFragmentContainer,
                setPasswordFragment, SetPasswordFragment.TAG).commit();
        return true;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
