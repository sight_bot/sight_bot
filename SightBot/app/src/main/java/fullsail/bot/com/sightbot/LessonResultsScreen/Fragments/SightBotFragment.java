package fullsail.bot.com.sightbot.LessonResultsScreen.Fragments;

import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import fullsail.bot.com.sightbot.CustomClasses.LessonProgressCustomClass;
import fullsail.bot.com.sightbot.R;


public class SightBotFragment extends Fragment {

    private static final String TAG = "SightBotFragment.TAG";

    public static SightBotFragment newInstance() {
        return new SightBotFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_completion_sight_bot, container, false);
        VideoView videoHolder = (VideoView) view.findViewById(R.id.sightBotCompletionVideoView);

        Uri videoUri = Uri.parse("android.resource://" + getActivity().getPackageName() + "/"
                + R.raw.correct);
        videoHolder.setVideoURI(videoUri);
        videoHolder.start();
        return view;
    }

}
