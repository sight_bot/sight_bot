package fullsail.bot.com.sightbot.LessonScreen;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import java.util.ArrayList;

import fullsail.bot.com.sightbot.CustomClasses.KidCustomClass;
import fullsail.bot.com.sightbot.CustomClasses.LessonProgressCustomClass;
import fullsail.bot.com.sightbot.DataHelper;
import fullsail.bot.com.sightbot.GlobalVariables;
import fullsail.bot.com.sightbot.LessonResultsScreen.LessonResultsActivity;
import fullsail.bot.com.sightbot.LessonScreen.Fragments.LessonProgressFragment;
import fullsail.bot.com.sightbot.LessonScreen.Fragments.SightBotAnimationFragment;
import fullsail.bot.com.sightbot.LessonScreen.Fragments.TimerFragment;
import fullsail.bot.com.sightbot.LessonScreen.Fragments.WordFlashcardFragment;
import fullsail.bot.com.sightbot.R;

public class LessonActivity extends AppCompatActivity implements WordFlashcardFragment.WordFlashcardFragmentInterface,
        TimerFragment.TimerInterface, LessonProgressFragment.LessonProgressInterface, TextToSpeech.OnInitListener, SightBotAnimationFragment.AnimationFinishedInterface, View.OnClickListener {

    private SpeechRecognizer mSpeechRecognizer;
    private Intent mSpeechRecognizerIntent;
    TimesUp timesUpReceiver;
    protected long mSpeechRecognizerStartListeningTime = 0;
    private boolean mSuccess;
    TextToSpeech tts;
    boolean mIsListening;
    boolean animationFinished = false;
    boolean kidSkippedTheInstructions = false;
    boolean wordCorrect;
    boolean saveInstantStateCalled = false;
    public static final String TAG = "LessonActivity.TAG";
    KidCustomClass kid;
    LessonProgressCustomClass lessonProgress;


    //Lifecycle methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AudioManager audioManager=(AudioManager)getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson);
        kid = (KidCustomClass) getIntent().getSerializableExtra(GlobalVariables.CHILD_EXTRA);
        lessonProgress = new LessonProgressCustomClass(kid);
        getProgressBar();
        WordFlashcardFragment flashcardFragment = WordFlashcardFragment.newInstance(lessonProgress, false, true);
        getFragmentManager().beginTransaction().replace(R.id.wordFlashcardFragment_container, flashcardFragment).commit();
        loadTimerFragment(false);
        loadSightBotAnimationFragment(GlobalVariables.ANIMATION_INSTRUCTIONS);

        //Making the lesson skippable
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.click_skip);
        linearLayout.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(GlobalVariables.ACTION_TIMES_UP);
        timesUpReceiver = new TimesUp();
        this.registerReceiver(timesUpReceiver, filter);
        if(animationFinished) {
            loadTimerFragment(true);
            createSpeechListener();
            createTextToSpeech();
        }
        if(saveInstantStateCalled){
            saveInstantStateCalled = false;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        saveInstantStateCalled = true;
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mSpeechRecognizer != null)
        {
            mSpeechRecognizer.stopListening();
            mSpeechRecognizer.cancel();
            mSpeechRecognizer.destroy();
            mSpeechRecognizer = null;
        }
        if(animationFinished) {
            unregisterReceiver(timesUpReceiver);
            tts.shutdown();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AudioManager audioManager=(AudioManager)getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
        mSpeechRecognizer = null;
        mSpeechRecognizerIntent = null;
        tts = null;
    }
    //End Lifecycle methods

    //Interfaces
    @Override
    public void flipCardInterface(boolean wordCorrect) {
        this.wordCorrect = wordCorrect;
        flipCard();
        getProgressBar();
    }

    @Override
    public void wordIncorrectInterface(String incorrectWord) {
        loadSightBotAnimationFragment(GlobalVariables.ANIMATION_INCORRECT);
        AudioManager audioManager=(AudioManager)getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
        tts.speak(lessonProgress.getWord(), TextToSpeech.QUEUE_FLUSH, null);
    }

    //Interface of
    @Override
    public void lessonComplete() {
        Intent lessonCompleteIntent = new Intent(this, LessonResultsActivity.class);
        lessonCompleteIntent.putExtra(GlobalVariables.EXTRA_LESSON_PROGRESS, lessonProgress);
        startActivity(lessonCompleteIntent);
        finish();
    }

    @Override
    public void timesUp() {
        Intent timesUpIntent = new Intent(GlobalVariables.ACTION_TIMES_UP);
        sendBroadcast(timesUpIntent);
    }

    @Override
    public void animationDidFinish() {
        if(!kidSkippedTheInstructions) {
            animationFinished = true;
            AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            audioManager.setStreamMute(AudioManager.STREAM_MUSIC, true);
            createTextToSpeech();
            createSpeechListener();
            loadTimerFragment(true);
            kid.setChildHasSeenLessonInstructions(true);
            DataHelper.updateKid(this, kid);
            kidSkippedTheInstructions = true;
        }
    }
    //End Interfaces

    //Listeners
    //Text to speech on Initialized listener
    @Override
    public void onInit(int status) {
    }

    protected class SpeechRecognitionListener implements RecognitionListener
    {

        @Override
        public void onBeginningOfSpeech()
        {
            Intent pauseTimer = new Intent(GlobalVariables.ACTION_PAUSE_TIMER);
            sendBroadcast(pauseTimer);
        }

        @Override
        public void onBufferReceived(byte[] buffer)
        {

        }

        @Override
        public void onEndOfSpeech()
        {
//            Log.d(TAG, "onEndOfSpeech");
        }

        @Override
        public void onError(int error)
        {
//            Log.d(TAG, "error = " + error);

//            Log.i(TAG, this.hashCode() + " - onError:" + error);

// Sometime onError will get called after onResults so we keep a boolean to ignore error also
            if (mSuccess) {
//                Log.w(TAG, "Already success, ignoring error");
                return;
            }

            long duration = System.currentTimeMillis() - mSpeechRecognizerStartListeningTime;
            if (duration < 500 && error == SpeechRecognizer.ERROR_NO_MATCH) {
//                Log.w(TAG, "Doesn't seem like the system tried to listen at all. duration = " + duration + "ms. This might be a bug with onError and startListening methods of SpeechRecognizer");
//                Log.w(TAG, "Going to ignore the error");
                return;
            }
            mSpeechRecognizer.stopListening();
            mSpeechRecognizer.cancel();
            mSpeechRecognizer.destroy();
            mSpeechRecognizer = null;
            createSpeechListener();
        }

        @Override
        public void onEvent(int eventType, Bundle params)
        {

        }

        @Override
        public void onPartialResults(Bundle partialResults) {}

        @Override
        public void onReadyForSpeech(Bundle params)
        {
//            Log.d(TAG, "onReadyForSpeech"); //$NON-NLS-1$
        }

        public void onResults(Bundle results)
        {
            mSuccess = true;
            //Log.d(TAG, "onResults"); //$NON-NLS-1$
            ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            // matches are the return values of speech recognition engine
            // Use these values for whatever you wish to do
            if(matches != null) {
                if (matches.size() > 0) {

                    for (int i = 0; i < matches.size(); i++) {
                     Log.i(TAG, matches.get(i) + " =? " + lessonProgress.getWord());
                        if (matches.get(i).equals(lessonProgress.getWord())) {
                            sendBroadcast(new Intent("SL").putExtra("WC", true));
                            loadSightBotAnimationFragment(GlobalVariables.ANIMATION_CORRECT);
                            return;
                        } else if (lessonProgress.getWord().equals("a") && matches.get(i).equals("hey")) {
                            loadSightBotAnimationFragment(GlobalVariables.ANIMATION_CORRECT);
                            sendBroadcast(new Intent("SL").putExtra("WC", true));
                            return;
                        } else if (lessonProgress.getWord().equals("I") && matches.get(i).equals("hi")) {
                            loadSightBotAnimationFragment(GlobalVariables.ANIMATION_CORRECT);
                            sendBroadcast(new Intent("SL").putExtra("WC", true));
                            return;
                        } else if (lessonProgress.getWord().equals("to") && matches.get(i).equals("till") ||
                                lessonProgress.getWord().equals("to") && matches.get(i).equals("too") ||
                                lessonProgress.getWord().equals("to") && matches.get(i).equals("2")){
                            loadSightBotAnimationFragment(GlobalVariables.ANIMATION_CORRECT);
                            sendBroadcast(new Intent("SL").putExtra("WC", true));
                            return;
                        } else if (lessonProgress.getWord().equals("than") && matches.get(i).equals("van")){
                            loadSightBotAnimationFragment(GlobalVariables.ANIMATION_CORRECT);
                            sendBroadcast(new Intent("SL").putExtra("WC", true));
                        }
                    }
                    sendBroadcast(new Intent("SL").putExtra("WC", false));
                } else {
                    sendBroadcast(new Intent("SL").putExtra("WC", false));
                }
            }
        }

        @Override
        public void onRmsChanged(float rmsdB)
        {

        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.click_skip) {
            if (kid.isChildHasSeenLessonInstructions()) {
                Intent stopInstructionsIntent = new Intent(GlobalVariables.BROADCAST_SKIP_INSTRUCTIONS_ANIMATION);
                sendBroadcast(stopInstructionsIntent);
                animationDidFinish();
            }
        }
    }
    //End Listeners

    //Load Fragment Methods
    private void loadTimerFragment(boolean animationCompleted){
        getFragmentManager().beginTransaction().replace(R.id.timerFragment_container, TimerFragment.newInstance(animationCompleted)).commit();
    }

    private void loadSightBotAnimationFragment(int animationType){
        getFragmentManager().beginTransaction().replace(R.id.sightBotAnimationFragment_container, SightBotAnimationFragment.newIntance(animationType)).commit();
    }

    private void getProgressBar(){
        LessonProgressFragment progressFragment = LessonProgressFragment.newInstance(lessonProgress);
        getFragmentManager().beginTransaction().replace(R.id.lessonProgressFragment_container, progressFragment).commit();
    }

    private void flipCard() {
        // Create and commit a new fragment transaction that adds the fragment for
        // the back of the card, uses custom animations, and is part of the fragment
        // manager's back stack.
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamMute(AudioManager.STREAM_MUSIC, true);
        WordFlashcardFragment flashcardFragment = WordFlashcardFragment.newInstance(lessonProgress, wordCorrect, false);
        if (!saveInstantStateCalled) {
            getFragmentManager()
                    .beginTransaction()
                            // Replace the default fragment animations with animator resources
                            // representing rotations when switching to the back of the card, as
                            // well as animator resources representing rotations when flipping
                            // back to the front (e.g. when the system Back button is pressed).
                    .setCustomAnimations(
                            R.animator.card_flip_right_in,
                            R.animator.card_flip_right_out,
                            R.animator.card_flip_left_in,
                            R.animator.card_flip_left_out)

                            // Replace any fragments currently in the container view with a
                            // fragment representing the next page (indicated by the
                            // just-incremented currentPage variable).
                    .replace(R.id.wordFlashcardFragment_container, flashcardFragment)

                            // Commit the transaction.
                    .commit();
            if (animationFinished) {
                loadTimerFragment(true);
                mSuccess = false;
                speechRecognizerStartListening(mSpeechRecognizerIntent);
            }
        }
    }
    //End Load Fragments

    //class methods
    protected synchronized void speechRecognizerStartListening(Intent intent) {
        if (mSpeechRecognizer != null) {
            AnimationUtils.loadAnimation(this, R.anim.pulsate);
            mSpeechRecognizerStartListeningTime = System.currentTimeMillis();
            this.mSpeechRecognizer.startListening(intent);
        }
    }

    private void createSpeechListener(){
        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
                this.getPackageName());
        SpeechRecognitionListener listener = new SpeechRecognitionListener();
        mSpeechRecognizer.setRecognitionListener(listener);

        if(!mIsListening) {
            mSpeechRecognizer.cancel();
            speechRecognizerStartListening(mSpeechRecognizerIntent);
        }
    }

    private void createTextToSpeech(){
        tts = new TextToSpeech(this, this);
        tts.setPitch(0.8f);
        tts.setSpeechRate(0.8f);
    }
    //End class Methods

    //Overriding the back button so it is disabled
    @Override
    public void onBackPressed() {
    }
    //End Override back button

    //Broadcast Receiver
    private class TimesUp extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            mSpeechRecognizer.stopListening();
            sendBroadcast(new Intent("SL").putExtra("WC", false));
        }
    }
    //End Broadcast Receiver

}
