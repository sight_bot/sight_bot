package fullsail.bot.com.sightbot.SettingsScreen.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Vibrator;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;
import fullsail.bot.com.sightbot.DataHelper;
import fullsail.bot.com.sightbot.GlobalVariables;
import fullsail.bot.com.sightbot.R;

/**
 * Created by davidfermer on 3/10/16.
 */
public class ChangePasswordFragment extends Fragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    public ChangePasswordFragment() {
    }

    public static ChangePasswordFragment newInstance(){
        return new ChangePasswordFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_password_reset_password, container, false);
        v.findViewById(R.id.cancel_resetPassword).setOnClickListener(this);
        v.findViewById(R.id.change_password_done_botton).setOnClickListener(this);
        ((CheckBox)v.findViewById(R.id.checkboxShowPassword)).setOnCheckedChangeListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.cancel_resetPassword){

            //Password reset was canceled. Reloading the details fragment for the first kid.
            reloadFirstKid();
        } else if(v.getId() == R.id.change_password_done_botton){

            //Check if passwords match between first text field and second text field
            if(!((EditText)getActivity().findViewById(R.id.first_password_field_reset_password))
                    .getText().toString()
                    .equals(((EditText) getActivity()
                            .findViewById(R.id.second_password_field_reset_password))
                            .getText().toString())){

                //Passwords don't match
                Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                Toast.makeText(getActivity(), "Your passwords do not match", Toast.LENGTH_SHORT).show();
                vibrator.vibrate(500);
            } else {

                //Passwords do match - save them out to preferences and reload the details fragment for the first kid
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences(GlobalVariables.Password, Context.MODE_PRIVATE);
                sharedPreferences.edit().putString(GlobalVariables.Password,
                        ((EditText) getActivity().findViewById(R.id.first_password_field_reset_password)).getText().toString()).apply();
                reloadFirstKid();
            }


        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked){
            ((EditText)getActivity().findViewById(R.id.first_password_field_reset_password)).setInputType(InputType.TYPE_CLASS_TEXT);
            ((EditText)getActivity().findViewById(R.id.second_password_field_reset_password)).setInputType(InputType.TYPE_CLASS_TEXT);
        } else {
            ((EditText)getActivity().findViewById(R.id.first_password_field_reset_password)).setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);
            ((EditText)getActivity().findViewById(R.id.second_password_field_reset_password)).setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
    }

    private void reloadFirstKid(){
        SettingsDetailFragment fragment = SettingsDetailFragment.newInstance(DataHelper.getAllKids(getActivity()).get(0));
        getFragmentManager().beginTransaction().replace(R.id.details, fragment).commit();
    }
}

