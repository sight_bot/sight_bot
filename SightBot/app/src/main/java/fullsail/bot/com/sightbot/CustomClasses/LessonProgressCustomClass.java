package fullsail.bot.com.sightbot.CustomClasses;

import android.content.Context;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

import fullsail.bot.com.sightbot.DataHelper;
import fullsail.bot.com.sightbot.GlobalVariables;

/**
 * Created by davidfermer on 3/13/16.
 */
public class LessonProgressCustomClass implements Serializable {

    private KidCustomClass kid;
    private ArrayList<LessonWordProgress> lessonWordProgresses;
    private ArrayList<LessonWordProgress> completedWordProgresses;
    private int originalNumberOfWords;
    private int wordsRemaining;
    public static final String TAG = "LessProgCustClass.TAG";

    public LessonProgressCustomClass(KidCustomClass kid) {
        this.kid = kid;
        ArrayList<String> words = getWords(kid.getCurrentQuarter(),kid.getCurrentWeek());
        lessonWordProgresses = new ArrayList<>();
        for (String word : words){
            lessonWordProgresses.add(new LessonWordProgress(word));
        }
        originalNumberOfWords = lessonWordProgresses.size();
        wordsRemaining = originalNumberOfWords;
        completedWordProgresses = new ArrayList<>();
        randomizeCards();
    }

    public ArrayList<LessonWordProgress> getCompletedWordProgresses() {
        return completedWordProgresses;
    }

    public int getOriginalNumberOfWords() {
        return originalNumberOfWords;
    }

    public int getNumberOfWordsRemaining(){
        return originalNumberOfWords - lessonWordProgresses.size();
    }

    public KidCustomClass getKid() {
        return kid;
    }

    public int getWordsRemaining() {
        return wordsRemaining;
    }

    public int getPercentage(){
        float totalTimesSeenAllWords = 0;
        float totalTimesCorrectAllWords = 0;
        for (LessonWordProgress singleProgress : completedWordProgresses){
            totalTimesCorrectAllWords = totalTimesCorrectAllWords + singleProgress.numberOfTimesCorrectThisSession;
            totalTimesSeenAllWords = totalTimesSeenAllWords + singleProgress.numberOfTimesSeenThisSession;
        }

        float test = Math.round(totalTimesCorrectAllWords / totalTimesSeenAllWords * 100);

        return (int)test;
    }

    public String getWord(){
        if (lessonWordProgresses.size() != 0){
            return lessonWordProgresses.get(0).word;
        } else {
            return null;
        }
    }
    public String wordAttempted(String word, boolean wasCorrect, Context context){
        kid.setSpecificWordProgress(word,wasCorrect);
        DataHelper.updateKid(context,kid);
        for(LessonWordProgress wordProgress : lessonWordProgresses){
            if(wordProgress.word.equals(word)){
                wordProgress.wordAttempted(wasCorrect);
                randomizeCards();
                return getWord();
            }
        }
        return null;
    }
    private void removeWordFromList(String word){
        for (int i = 0; i < lessonWordProgresses.size(); i ++){
            if(word.equals(lessonWordProgresses.get(i).word)) {
                completedWordProgresses.add(lessonWordProgresses.remove(i));
            }
        }
    }
    private ArrayList<String> getWords(int quarter, int week){
        ArrayList<String> words = new ArrayList<>();

        int numberOfWordsToAdd = 0;

        if (week <= 5){
            numberOfWordsToAdd = week*5;
        } else {
            numberOfWordsToAdd = 25;
        }

        if(quarter == 1){
            for (int i = 0; i < numberOfWordsToAdd; i++){
                words.add(GlobalVariables.wordsFromFirstQuarter[i]);
            }
        } else if (quarter == 2) {
            for (int i = 0; i < 25; i++){
                words.add(GlobalVariables.wordsFromFirstQuarter[i]);
            }
            for (int i = 0; i < numberOfWordsToAdd; i++){
                words.add(GlobalVariables.wordsFromSecondQuarter[i]);
            }
        } else if (quarter == 3) {
            for (int i = 0; i < 25; i++){
                words.add(GlobalVariables.wordsFromFirstQuarter[i]);
            }
            for (int i = 0; i < 25; i++){
                words.add(GlobalVariables.wordsFromSecondQuarter[i]);
            }
            for (int i = 0; i < numberOfWordsToAdd; i++){
                words.add(GlobalVariables.wordsFromThirdQuarter[i]);
            }
        } else if (quarter == 4) {
            for (int i = 0; i < 25; i++){
                words.add(GlobalVariables.wordsFromFirstQuarter[i]);
            }
            for (int i = 0; i < 25; i++){
                words.add(GlobalVariables.wordsFromSecondQuarter[i]);
            }
            for (int i = 0; i < 25; i++){
                words.add(GlobalVariables.wordsFromThirdQuarter[i]);
            }
            for (int i = 0; i < numberOfWordsToAdd; i++){
                words.add(GlobalVariables.wordsFromFourthQuarter[i]);
            }
        }

        return words;
    }

    public class LessonWordProgress implements Serializable{
        public String word;
        public boolean firstCorrect;
        public boolean secondCorrect;
        public boolean thirdCorrect;
        public int numberOfTimesSeenThisSession;
        public int numberOfTimesCorrectThisSession;


        public LessonWordProgress(String word) {
            this.word = word;
            this.firstCorrect = false;
            this.secondCorrect = false;
            this.thirdCorrect = false;
            numberOfTimesCorrectThisSession = 0;
            numberOfTimesSeenThisSession = 0;
        }

        public void wordAttempted(boolean isCorrect){
            ++numberOfTimesSeenThisSession;
            if(isCorrect && numberOfTimesSeenThisSession == 1){
                ++numberOfTimesCorrectThisSession;
                removeWordFromList(word);
                --wordsRemaining;
                return;
            }
            if(!isCorrect){
                firstCorrect = false;
                secondCorrect = false;
                thirdCorrect = false;
            } else {
                ++numberOfTimesCorrectThisSession;
                if(!firstCorrect){
                    firstCorrect = true;
                } else if (!secondCorrect){
                    secondCorrect = true;
                } else {
                    //Three times correct removing this word from the array of words.
                    removeWordFromList(word);
                    --wordsRemaining;
                }
            }
        }

    }

    private void randomizeCards() {
        ArrayList<LessonWordProgress> lessonWordProgresses = this.lessonWordProgresses;
        ArrayList<LessonWordProgress> newLessonWordProgresses = new ArrayList<>();
        if (lessonWordProgresses.size() != 1) {
            for (int i = 0; i < wordsRemaining; i++) {
                int randomNumber = randomNumberGenerator(lessonWordProgresses.size());
                if (i == 0) {
                    do {
                        if (newLessonWordProgresses.size() > 0) {
                            newLessonWordProgresses.remove(0);
                            randomNumber = randomNumberGenerator(lessonWordProgresses.size());
                        }
                        newLessonWordProgresses.add(lessonWordProgresses.get(randomNumber));
                    }
                    while (newLessonWordProgresses.get(0).word.equals(lessonWordProgresses.get(0).word));

                } else {
                    newLessonWordProgresses.add(lessonWordProgresses.get(randomNumber));
                }
                lessonWordProgresses.remove(randomNumber);
            }
            this.lessonWordProgresses = newLessonWordProgresses;
        }
    }


    private int randomNumberGenerator(int numberOfWordsStillNeedingRandomNumber){
        Random randomGenerator = new Random();
        return randomGenerator.nextInt(numberOfWordsStillNeedingRandomNumber);
    }
}
