package fullsail.bot.com.sightbot.SettingsScreen;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import fullsail.bot.com.sightbot.CustomClasses.KidCustomClass;
import fullsail.bot.com.sightbot.DataHelper;
import fullsail.bot.com.sightbot.GlobalVariables;
import fullsail.bot.com.sightbot.R;
import fullsail.bot.com.sightbot.SettingsScreen.Fragments.SettingsChildFragment;
import fullsail.bot.com.sightbot.SettingsScreen.Fragments.SettingsDetailFragment;
import fullsail.bot.com.sightbot.SettingsScreen.Fragments.SettingsEditDetailFragment;

public class SettingsActivity extends AppCompatActivity {

    public static final String TAG = "SettingsActivity.TAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        SettingsChildFragment frag = SettingsChildFragment.newInstance(DataHelper.getAllKids(this));
        getFragmentManager().beginTransaction().replace(R.id.children, frag, SettingsChildFragment.TAG).commit();

        //TODO - pass a kidCustomClass through this newInstance if editing.  If creating a new child put null.
        if (DataHelper.getAllKids(this).size() == 0) {
            setTitle("Edit Child Details");
            SettingsEditDetailFragment frag2 = SettingsEditDetailFragment.newInstance(null);
            getFragmentManager().beginTransaction().replace(R.id.details, frag2, SettingsEditDetailFragment.TAG).commit();
        } else {
            setTitle("Child Details");
            SettingsDetailFragment settingsDetailFragment = SettingsDetailFragment.newInstance(DataHelper.getAllKids(this).get(0));
            getFragmentManager().beginTransaction().replace(R.id.details, settingsDetailFragment).commit();
        }
    }

    public void childDetailsSaved(KidCustomClass kid) {
        setTitle("Child Details");
        SettingsChildFragment frag = SettingsChildFragment.newInstance(DataHelper.getAllKids(this));
        getFragmentManager().beginTransaction().replace(R.id.children, frag, SettingsChildFragment.TAG).commit();
        SettingsDetailFragment frag2 = SettingsDetailFragment.newInstance(kid);
        getFragmentManager().beginTransaction().replace(R.id.details, frag2, SettingsDetailFragment.TAG).commit();
    }

    public void showDetails(KidCustomClass kidCustomClass) {
        SettingsDetailFragment frag2 = SettingsDetailFragment.newInstance(kidCustomClass);
        getFragmentManager().beginTransaction().replace(R.id.details, frag2, SettingsDetailFragment.TAG).commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode != Activity.RESULT_CANCELED) {
            ImageView imageView = (ImageView) findViewById(R.id.detailImage);

            if (data != null) {
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                imageView.setImageBitmap(imageBitmap);
                data.setAction(GlobalVariables.BROADCAST_IMAGE_TAKEN);
                sendBroadcast(data);
            }

        } else if (requestCode == 50 && resultCode != Activity.RESULT_CANCELED) {
            Uri selectedImage = data.getData();

            String[] filePathColumn = {
                    MediaStore.Images.Media.DATA
            };

            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String imgDecodableString = cursor.getString(columnIndex);
            cursor.close();
            Bitmap test = BitmapFactory.decodeFile(imgDecodableString);
            ((ImageView) findViewById(R.id.detailImage)).setImageBitmap(test);
            data = new Intent(GlobalVariables.BROADCAST_IMAGE_TAKEN);
            data.putExtra(GlobalVariables.EXTRA_IMAGE_LOCATION_AS_STRING, imgDecodableString);
            sendBroadcast(data);

        }

    }
}
