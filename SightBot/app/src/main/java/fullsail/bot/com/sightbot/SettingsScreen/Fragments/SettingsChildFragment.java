package fullsail.bot.com.sightbot.SettingsScreen.Fragments;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import fullsail.bot.com.sightbot.CustomClasses.KidCustomClass;
import fullsail.bot.com.sightbot.SettingsScreen.Adapters.ChildListAdapter;
import fullsail.bot.com.sightbot.SettingsScreen.SettingsActivity;

/**
 * Created by snipetysnipes on 3/7/16.
 */
public class SettingsChildFragment extends ListFragment implements View.OnTouchListener {

    public static final String TAG = "LoginFragment.TAG";
    public static final String ARG = "LoginFragment.ARG";
    ArrayList<String> kcc = new ArrayList<>();
    ArrayList<KidCustomClass> kids = new ArrayList<>();

    public static SettingsChildFragment newInstance(ArrayList<KidCustomClass> allKids){
        SettingsChildFragment frag = new SettingsChildFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG, allKids);
        frag.setArguments(args);

        return frag;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        kcc.add("No Data");
        Bundle args = getArguments();

        if (args != null) {
            kids = (ArrayList<KidCustomClass>) args.getSerializable(ARG);

            ChildListAdapter listAdapter = new ChildListAdapter(getActivity(), kids);
            setListAdapter(listAdapter);
            listAdapter.notifyDataSetChanged();
        }  else {
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,  kcc);
            setListAdapter(dataAdapter);
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        ((SettingsActivity)getActivity()).showDetails(kids.get(position));

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        v.getParent().requestDisallowInterceptTouchEvent(true);
        return false;
    }
}
