package fullsail.bot.com.sightbot.LoginScreen;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import fullsail.bot.com.sightbot.CustomClasses.ImageCustomClass;
import fullsail.bot.com.sightbot.CustomClasses.KidCustomClass;
import fullsail.bot.com.sightbot.DataHelper;
import fullsail.bot.com.sightbot.GlobalVariables;
import fullsail.bot.com.sightbot.LessonResultsScreen.SendProgressEmailService;
import fullsail.bot.com.sightbot.LessonScreen.Fragments.SightBotAnimationFragment;
import fullsail.bot.com.sightbot.LessonScreen.LessonActivity;
import fullsail.bot.com.sightbot.LoginScreen.Fragments.CreateAccFragment;
import fullsail.bot.com.sightbot.LoginScreen.Fragments.LoginFragment;
import fullsail.bot.com.sightbot.LoginScreen.Fragments.SightBotLoginAnimationFragment;
import fullsail.bot.com.sightbot.R;

public class LoginActivity extends AppCompatActivity{

    SharedPreferences prefs = null;
    ImageView arrow;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
            getPermissions();
        loadSightBotLoginAnimationFragment();
    }

    private void loadSightBotLoginAnimationFragment(){
        getFragmentManager().beginTransaction().replace(R.id.sight_bot_login_container, SightBotLoginAnimationFragment.newIntance()).commit();
    }


    @Override
    protected void onResume() {
        super.onResume();

        arrow = (ImageView) findViewById(R.id.imageView2);
        mContext = this;

        Animation animSlide = AnimationUtils.loadAnimation(this,
                R.anim.loop);
        arrow.startAnimation(animSlide);

        prefs = getSharedPreferences("fullsail.bot.com.sightbot", MODE_PRIVATE);
        LinearLayout scrollContainer = (LinearLayout) findViewById(R.id.scroll_container);
        scrollContainer.removeAllViews();

        if (DataHelper.getAllKids(this).size() != 0) {
            final ArrayList<KidCustomClass> kids = DataHelper.getAllKids(this);

            for (int i = 0; i < kids.size(); i++) {
                final ImageView childImage = new ImageView(this);
                final TextView childName = new TextView(this);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ImageCustomClass imageCustomClass = DataHelper.getSpecificImage(this,kids.get(i).getChildName());
                    childImage.setImageBitmap(imageCustomClass.getChildImage());
                }

                childImage.setScaleType(ImageView.ScaleType.FIT_CENTER);

                childName.setText(kids.get(i).getChildName());
                childName.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                childName.setTextColor(getResources().getColor(R.color.primary_text_color));


                scrollContainer = (LinearLayout) findViewById(R.id.scroll_container);
                ((LinearLayout) findViewById(R.id.sight_bot_login_container)).setVisibility(View.VISIBLE);



                LinearLayout.LayoutParams tlp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 50);

                LinearLayout buttonLayout = new LinearLayout(this);
                buttonLayout.setBackground(getDrawable(R.drawable.sightbot_card));
                buttonLayout.setOrientation(LinearLayout.VERTICAL);
                buttonLayout.setLayoutParams(new LinearLayout.LayoutParams(400, 400));

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(300, 300);

                buttonLayout.addView(childImage, lp);
                buttonLayout.addView(childName, tlp);
                scrollContainer.addView(buttonLayout);

                childImage.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 300));
                childImage.setPadding(30, 30, 30, 15);

                buttonLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, LessonActivity.class);
                        intent.putExtra(GlobalVariables.CHILD_EXTRA,DataHelper.getSpecificKid(mContext,childName.getText().toString()));
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    }
                });

            }
        } else {
            ((LinearLayout) findViewById(R.id.sight_bot_login_container)).setVisibility(View.GONE);
        }

        HorizontalScrollView InitScrollView = (HorizontalScrollView) findViewById(R.id.horizontalScrollView);
        InitScrollView.setSmoothScrollingEnabled(true);

        if (prefs.getBoolean("firstrun", true)) {
            prefs.edit().putBoolean("firstrun", false).apply();
        } else {
            findViewById(R.id.hint).setVisibility(View.GONE);
            arrow.clearAnimation();
            arrow.setVisibility(View.GONE);
        }
        if (DataHelper.getAllKids(this).size() == 0) {
            findViewById(R.id.hint).setVisibility(View.VISIBLE);
            arrow.setVisibility(View.VISIBLE);
            arrow.startAnimation(animSlide);
            LoginFragment frag = LoginFragment.newInstance();
            getFragmentManager().beginTransaction().replace(R.id.container, frag).commit();
        } else {
            findViewById(R.id.hint).setVisibility(View.GONE);
            arrow.clearAnimation();
            arrow.setVisibility(View.GONE);
            CreateAccFragment frag = CreateAccFragment.newInstance();
            getFragmentManager().beginTransaction().replace(R.id.container, frag).commit();
        }
    }
// TODO - Do we need this anymore? - Dave doesn't need it.  Logan doesn't need it either.
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.settings_action, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        SetPasswordFragment setPasswordFragment = SetPasswordFragment.newInstance();
//        findViewById(R.id.hint).setVisibility(View.GONE);
//        findViewById(R.id.imageView2).setVisibility(View.GONE);
//        findViewById(R.id.passwordFragmentContainer).setVisibility(View.VISIBLE);
//        getFragmentManager().beginTransaction().replace(R.id.passwordFragmentContainer,
//                setPasswordFragment, SetPasswordFragment.TAG).commit();
//        return true;
//    }

    @Override
    public void onBackPressed() {

    }

    private void getPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                    PackageManager.PERMISSION_GRANTED ) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PackageManager.PERMISSION_GRANTED);
                Toast.makeText(this,"We need access to storage to save your child's progress", Toast.LENGTH_LONG).show();
            }
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO},
                        PackageManager.PERMISSION_GRANTED);
                Toast.makeText(this,"We need access to the microphone to hear your child", Toast.LENGTH_LONG).show();
            }
        }
    }
}
