package fullsail.bot.com.sightbot.CustomClasses;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;

/**
 * Created by davidfermer on 3/18/16.
 */
public class ImageCustomClass implements Serializable {

    private byte[] childImage;
    boolean imageIsFromCamera;
    String childName;

    public ImageCustomClass(Bitmap image, boolean imageIsFromCamera, String childName) {
        setChildImage(image);
        this.imageIsFromCamera = imageIsFromCamera;
        this.childName = childName;
    }

    private void setChildImage(Bitmap image) {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, stream);
        childImage = stream.toByteArray();
    }

    public Bitmap getChildImage() {

        if (!imageIsFromCamera) {
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inSampleSize = 10;
            return BitmapFactory.decodeByteArray(childImage, 0, childImage.length, opts);
        } else {
            return BitmapFactory.decodeByteArray(childImage, 0, childImage.length);
        }
    }

    public String getChildName() {
        return childName;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof ImageCustomClass){
            ImageCustomClass imageBeingPassedIn = (ImageCustomClass) o;
            return imageBeingPassedIn.getChildName().equals(childName);
        } else {
            return false;
        }
    }
}
