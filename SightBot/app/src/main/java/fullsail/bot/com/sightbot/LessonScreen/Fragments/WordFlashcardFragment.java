package fullsail.bot.com.sightbot.LessonScreen.Fragments;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import fullsail.bot.com.sightbot.CustomClasses.LessonProgressCustomClass;
import fullsail.bot.com.sightbot.R;

/**
 * Created by davidfermer on 3/13/16.
 */

public class WordFlashcardFragment extends Fragment {

    public static final String TAG = "WordFlashFrag.TAG";

    SpeechReceiver speechReceiver;
    WordFlashcardFragmentInterface wordFlashcardFragmentInterface;
    LessonProgressCustomClass lessonProgress;
    LinearLayout cardLayout;

    public WordFlashcardFragment() {}

    public static WordFlashcardFragment newInstance(LessonProgressCustomClass lessonProgress, boolean wordCorrect, boolean firstTimeCreated) {
        WordFlashcardFragment frag = new WordFlashcardFragment();
        Bundle args = new Bundle();
        args.putSerializable("LP", lessonProgress);
        args.putBoolean("WC", wordCorrect);
        args.putBoolean("firstTimeCreated", firstTimeCreated);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter("SL");
        speechReceiver = new SpeechReceiver();
        getActivity().registerReceiver(speechReceiver, intentFilter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        wordFlashcardFragmentInterface = (WordFlashcardFragmentInterface)getActivity();
        return inflater.inflate(R.layout.frag_word_flashcard, container, false);
    }


    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(speechReceiver);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();
        lessonProgress = (LessonProgressCustomClass) bundle.getSerializable("LP");
        prepareWord();

        if (bundle.getBoolean("WC")) {
            wordChange(true);
        } else if (!getArguments().getBoolean("firstTimeCreated")){
            wordChange(false);
        }
    }

    public void nextWord(boolean wordCorrect) {
        if (wordCorrect) {
            animateCardFlip(wordCorrect);
            cardLayout = (LinearLayout) getActivity().findViewById(R.id.cardLayout);
        } else {
            wordIncorrect();
        }
    }

    // Prepares the first word.
    public void prepareWord() {
        ((TextView) getView().findViewById(R.id.currentWord)).setText(lessonProgress.getWord());

    }

    public void wordChange(boolean wordCorrect) {
        ((TextView) getView().findViewById(R.id.currentWord)).setText
                (lessonProgress.wordAttempted(((TextView) getView().findViewById(R.id.currentWord))
                        .getText().toString(), wordCorrect, getActivity()));


    }

    public void animateCardFlip(boolean wordCorrect) {
        wordFlashcardFragmentInterface.flipCardInterface(wordCorrect);
    }

    public void wordIncorrect() {
        wordFlashcardFragmentInterface.wordIncorrectInterface(lessonProgress.getWord());
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.slide);
        animation.reset();
        getView().findViewById(R.id.flashcard).clearAnimation();
        getView().findViewById(R.id.flashcard).startAnimation(animation);

        new CountDownTimer(2000, 1000) {

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                wordFlashcardFragmentInterface.flipCardInterface(false);
            }

        }.start();
    }

    public interface WordFlashcardFragmentInterface {
        void flipCardInterface(boolean wordCorrect);
        void wordIncorrectInterface(String incorrectWord);
    }

    private class SpeechReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            nextWord(intent.getBooleanExtra("WC", false));
        }
    }
}
