package fullsail.bot.com.sightbot.CustomClasses;

import java.io.Serializable;
import java.util.ArrayList;

import fullsail.bot.com.sightbot.GlobalVariables;

/**
 * Created by davidfermer on 3/4/16.
 */
public class KidCustomClass implements Serializable {

    //Child Details set at creation and can be edited.
    private String childName;
    private String gender;
    private String grade;
    private String teacherName;
    private String teacherEmail;
    private int currentWeek;
    private int currentQuarter;
    private boolean parentEmailUtilized;
    //This Multidimensional Array is where you pull all the child progress from.
    private ArrayList<ArrayList<WordProgress>> childProgress;
    private String teacherTitle;
    private String teacherNameWithoutTitle;
    private boolean childHasSeenLessonInstructions;

    public KidCustomClass(String childName, String gender, String grade, String teacherName,
                          String teacherEmail, int currentWeek, int currentQuarter, boolean parentEmailUtilized, String teacherTitle, String teacherNameWithoutTitle/*, Bitmap image, boolean imageIsFromCamera*/) {
        this.childName = childName;
        this.gender = gender;
        this.grade = grade;
        this.teacherName = teacherName;
        this.teacherEmail = teacherEmail;
        this.currentWeek = currentWeek;
        this.currentQuarter = currentQuarter;
        this.parentEmailUtilized = parentEmailUtilized;
        childProgress = new ArrayList<>();
        this.teacherTitle = teacherTitle;
        this.teacherNameWithoutTitle = teacherNameWithoutTitle;
        setInitialChildProgress();
    }

    //Getters
    public String getChildName() {
        return childName;
    }
    public String getGender() {
        return gender;
    }
    public String getGrade() {
        return grade;
    }
    public String getTeacherName() {
        return teacherName;
    }
    public String getTeacherEmail() {
        return teacherEmail;
    }
    public int getCurrentWeek() {
        return currentWeek;
    }
    public int getCurrentQuarter() {
        return currentQuarter;
    }
    public boolean getIsParentEmailUtilized() {
        return parentEmailUtilized;
    }
    public boolean isParentEmailUtilized() {
        return parentEmailUtilized;
    }
    public String getTeacherTitle() {
        return teacherTitle;
    }
    public boolean isChildHasSeenLessonInstructions() {
        return childHasSeenLessonInstructions;
    }

    public ArrayList<ArrayList<WordProgress>> getChildProgress() {
        return childProgress;
    }

    //Use this function to erase the child's progress.
    public void eraseChildsProgress(){
        childProgress.clear();
        setInitialChildProgress();
    }

    //This method is called initially when the child is created and any time the child's progress
    // is erased
    private void setInitialChildProgress(){

        //Set up for tracking child's progress
        ArrayList<WordProgress> firstQuarterWordProgresses = new ArrayList<>();
        for (String word : GlobalVariables.wordsFromFirstQuarter){
            firstQuarterWordProgresses.add(new WordProgress(word));
        }
        childProgress.add(firstQuarterWordProgresses);
        ArrayList<WordProgress> secondQuarterWordProgresses = new ArrayList<>();
        for (String word : GlobalVariables.wordsFromSecondQuarter){
            secondQuarterWordProgresses.add(new WordProgress(word));
        }
        childProgress.add(secondQuarterWordProgresses);
        ArrayList<WordProgress> thirdQuarterWordProgresses = new ArrayList<>();
        for (String word : GlobalVariables.wordsFromThirdQuarter){
            thirdQuarterWordProgresses.add(new WordProgress(word));
        }
        childProgress.add(thirdQuarterWordProgresses);
        ArrayList<WordProgress> fourthQuarterWordProgresses = new ArrayList<>();
        for (String word : GlobalVariables.wordsFromFourthQuarter){
            fourthQuarterWordProgresses.add(new WordProgress(word));
        }
        childProgress.add(fourthQuarterWordProgresses);
    }

    public void setNextLesson(int percentageOnPreviousLesson){
        if(percentageOnPreviousLesson >= 80){
            if(currentWeek == 9){
                if(currentQuarter != 4){
                    ++currentQuarter;
                    currentWeek = 1;
                }
            } else {
                ++currentWeek;
            }
        }
    }

    //Use this function each time the child sees a word in the lesson.
    public void setSpecificWordProgress(String word, boolean childGotItCorrect){
        for(ArrayList<WordProgress> wordProgresses : childProgress){
            for(WordProgress progress : wordProgresses){
                if(progress.getWord().equals(word)){
                    progress.childSawWord(childGotItCorrect);
                }
            }
        }
    }


    public void setChildHasSeenLessonInstructions(boolean childHasSeenLessonInstructions) {
        this.childHasSeenLessonInstructions = childHasSeenLessonInstructions;
    }

    public Double getQuarterPercentage (int quarter) {

        ArrayList<WordProgress> wordProgressArrayList = childProgress.get(quarter);

        Double totalPercentage = 0.0;

        for (int i = 0; i < wordProgressArrayList.size(); i++) {
            totalPercentage = totalPercentage + wordProgressArrayList.get(i).getPercentageCorrect();
        }

        return totalPercentage / wordProgressArrayList.size() * 100;
    }

    //This class is used to keep track of each word and the child's number of attempts at it
    public class WordProgress implements Serializable{
        private String word;
        private int numberOfTimesSeen;
        private int numberOfTimesCorrect;
        private double percentageCorrect;


        public WordProgress(String word) {
            this.word = word;
            numberOfTimesSeen = 0;
            numberOfTimesCorrect = 0;
            percentageCorrect = 1.0;
        }

        public String getWord() {
            return word;
        }

        public int getNumberOfTimesSeen() {
            return numberOfTimesSeen;
        }

        public int getNumberOfTimesCorrect() {
            return numberOfTimesCorrect;
        }

        public double getPercentageCorrect() {
            return percentageCorrect;
        }

        public void childSawWord(boolean gotItCorrect){
            ++numberOfTimesSeen;
            if(gotItCorrect){
                ++numberOfTimesCorrect;
            }
            percentageCorrect = numberOfTimesCorrect/numberOfTimesSeen;
        }
    }

    public boolean checkKidNameEquals(String kidName) {
        return(kidName.equals(childName));
    }
}
