package fullsail.bot.com.sightbot.LoginScreen.Fragments;

import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;
import fullsail.bot.com.sightbot.R;

public class SightBotLoginAnimationFragment extends Fragment{
    VideoView videoHolder;

    public static final String TAG = "SightBotLoginAnimationFragment";
    private static final int MAIN_REQUEST_CODE = 0x00011;

    public static SightBotLoginAnimationFragment newIntance() {
        return new SightBotLoginAnimationFragment();
    }


    @Override
    public void onPause() {
        super.onPause();
        videoHolder.stopPlayback();
    }

    @Override
    public void onResume() {
        super.onResume();
        videoHolder.start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_login_sight_bot, container, false);
        videoHolder = (VideoView) view.findViewById(R.id.sightBotLoginVideoView);

        Uri videoUri = Uri.parse("android.resource://" + getActivity().getPackageName() + "/"
                + R.raw.talking);
        videoHolder.setVideoURI(videoUri);
        videoHolder.start();

        return view;
    }
}
