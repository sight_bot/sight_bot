package fullsail.bot.com.sightbot.LaunchScreen;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import fullsail.bot.com.sightbot.LaunchScreen.Fragments.LaunchScreenFragment;
import fullsail.bot.com.sightbot.R;

public class LaunchScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch_screen);
        callLaunchScreenFragment();
    }

    public void callLaunchScreenFragment(){
        LaunchScreenFragment fragment = (LaunchScreenFragment) getSupportFragmentManager().findFragmentByTag(LaunchScreenFragment.TAG);

        if (fragment == null){
            fragment = LaunchScreenFragment.newIntance();
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.launch_screen_container, fragment, LaunchScreenFragment.TAG).commit();
    }
}

