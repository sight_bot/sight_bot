package fullsail.bot.com.sightbot.LessonScreen.Fragments;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import fullsail.bot.com.sightbot.GlobalVariables;
import fullsail.bot.com.sightbot.R;

public class TimerFragment extends Fragment {

    TextView mTextField;
    ProgressBar pb;
    int count = 5;
    TimerInterface timerInterface;
    CountDownTimer countDownTimer;
    PauseTimerBroadcastReceiver receiver;
    boolean animationCompleted;

    public static TimerFragment newInstance(boolean animationCompleted){

        TimerFragment frag = new TimerFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable("animation", animationCompleted);
        frag.setArguments(bundle);

        return frag;
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(GlobalVariables.ACTION_PAUSE_TIMER);
        receiver = new PauseTimerBroadcastReceiver();
        getActivity().registerReceiver(receiver,filter);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        timerInterface = (TimerInterface) getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        timerInterface = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_timer, container, false);
        mTextField = (TextView) view.findViewById(R.id.mTextField);
        pb = (ProgressBar) view.findViewById(R.id.progressBarToday);
        animationCompleted = getArguments().getBoolean("animation");

        if (animationCompleted) {
            countDownTimer = new CountDownTimer(5000, 100) {
                public void onTick(long millisUntilFinished) {
                    int fix = 1000;
                    count = Integer.parseInt(String.valueOf((millisUntilFinished + fix) / 1000));
                    mTextField.setText(String.valueOf(count));
                    pb.setProgress(Integer.parseInt(String.valueOf(millisUntilFinished)));
                }

                public void onFinish() {
                    mTextField.setText("0");
                    pb.setProgress(5000);
                    timerInterface.timesUp();
                }
            }.start();
        }

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (animationCompleted) {
            countDownTimer.cancel();
        }
        getActivity().unregisterReceiver(receiver);
    }

    public interface TimerInterface{
        void timesUp();
    }


    private class PauseTimerBroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            if (countDownTimer != null) {
                countDownTimer.cancel();
            }
        }
    }
}
